# cvbuilderapp_facade

Code name `cvbuilderapp` is officially the first app or `micro saas`, developed by the entity soon to be official _**Blackbird**_.

`cvbuilderapp` will soon be launched under the commercial name **Cv-template.co**.

This project is the public pages of the website, landing page, auth pages, and share cv public links.
