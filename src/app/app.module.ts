import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { SharedModule, WebpackTranslateLoader } from '../shared/shared.module';
import { TranslationPathsService } from '../core/services/translation-paths.service';
import { PubSubService } from '../core/services/pub-sub.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TitleAndMetaService } from '../core/services/title-and-meta.service';
import { NetworkService } from '../core/services/network.service';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { InOutInterceptor } from '../core/services/in-out.interceptor';
import { UsersService } from '../core/services/users.service';
import { VerifyKeyGuard } from '../core/guards/verify-key.guard';
import { BuilderService } from '../core/services/builder.service';
import { NgcCookieConsentConfig, NgcCookieConsentModule } from 'ngx-cookieconsent';
import { environment } from '../environments/environment';
import { CookieService } from 'ngx-cookie-service';

const cookieConfig: NgcCookieConsentConfig = {
	cookie: {
		domain: environment.mainUrl // or 'your.domain.com' // it is mandatory to set a domain, for cookies to work properly (see https://goo.gl/S2Hy2A)}
	},
	autoOpen: false
};

@NgModule({
	declarations: [AppComponent],
	imports: [
		BrowserModule.withServerTransition({ appId: 'serverApp' }),
		AppRoutingModule,
		SharedModule,
		BrowserAnimationsModule,
		NgcCookieConsentModule.forRoot(cookieConfig),
		TranslateModule.forRoot({
			loader: {
				provide: TranslateLoader,
				useClass: WebpackTranslateLoader
			}
		})
	],
	providers: [
		TranslationPathsService,
		PubSubService,
		TitleAndMetaService,
		NetworkService,
		CookieService,
		UsersService,
		VerifyKeyGuard,
		BuilderService,
		{
			provide: HTTP_INTERCEPTORS,
			useClass: InOutInterceptor,
			multi: true
		}
	],
	bootstrap: [AppComponent]
})
export class AppModule {}
