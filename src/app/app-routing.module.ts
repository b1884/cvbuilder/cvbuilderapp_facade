import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { VerifyKeyGuard } from '../core/guards/verify-key.guard';
import { AuthGuard } from '../core/guards/auth.guard';

const appRoutes: Routes = [
	{
		path: '',
		runGuardsAndResolvers: 'always',
		canActivate: [VerifyKeyGuard],
		loadChildren: () => import('../features/landing/landing.module').then((value) => value.LandingModule)
	},
	{
		path: 's',
		loadChildren: () =>
			import('../features/public-cvlink/public-cvlink.module').then((value) => value.PublicCVLinkModule)
	},
	{
		path: 'auth',
		canActivate: [AuthGuard],
		loadChildren: () => import('../features/auth/auth.module').then((value) => value.AuthModule)
	},
	{
		path: '**',
		redirectTo: ''
	}
];

@NgModule({
	imports: [
		RouterModule.forRoot(appRoutes, {
			initialNavigation: 'enabledBlocking',
			onSameUrlNavigation: 'reload'
		})
	],
	exports: [RouterModule]
})
export class AppRoutingModule {}
