import { Component, Inject, Injector, OnDestroy, OnInit, PLATFORM_ID } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ServiceLocator } from '../shared/classes/service-locator.class';
import { CookieService } from 'ngx-cookie-service';
import { filter, lastValueFrom, Subject, takeUntil } from 'rxjs';
import { DOCUMENT, isPlatformBrowser } from '@angular/common';
import { NgcCookieConsentService } from 'ngx-cookieconsent';
import { environment } from '../environments/environment';
import routes from '../routes';
import { LangType } from '../types/lang.types';
import { TranslationPathsService } from '../core/services/translation-paths.service';
import { LANGUAGES } from '../core/config/enabled-languages.config';
import { NavigationEnd, Router } from '@angular/router';

declare const gtag: (event: string, type: string, url: { page_path: string }) => void;

@Component({
	selector: 'blackbird-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnDestroy, OnInit {
	private _onLangChange: Subject<void> = new Subject<void>();

	constructor(
		private _translateService: TranslateService,
		private _injector: Injector,
		private _cookieService: CookieService,
		private _ccService: NgcCookieConsentService,
		private _t: TranslationPathsService,
		private _router: Router,
		@Inject(DOCUMENT) private _doc: Document,
		@Inject(PLATFORM_ID) private _platformId: unknown
	) {
		ServiceLocator.injector = this._injector;
		const cookiesLang = _cookieService.get('x-client-language');
		_translateService.setDefaultLang(LANGUAGES.includes(cookiesLang) ? cookiesLang : 'en');

		if (isPlatformBrowser(_platformId as never) && environment.production) {
			_router.events.pipe(filter((event) => event instanceof NavigationEnd)).subscribe((event) => {
				gtag('event', 'page_view', {
					page_path: (event as NavigationEnd).urlAfterRedirects
				});
			});
		}
	}

	ngOnDestroy(): void {
		if (this._onLangChange) {
			this._onLangChange.next();
			this._onLangChange.complete();
		}
	}

	ngOnInit(): void {
		const currentLang = this._translateService.currentLang || this._translateService.defaultLang;
		this._doc.documentElement.lang = `${currentLang}`;
		this._doc.documentElement.dir = currentLang === 'ar' ? 'rtl' : 'ltr';

		setTimeout(() => {
			void this._initCookiesBanner();
		}, 1200);

		this._translateService.onLangChange.pipe(takeUntil(this._onLangChange)).subscribe(() => {
			const lang = this._translateService.currentLang || this._translateService.defaultLang;
			this._doc.documentElement.lang = `${lang}`;

			const dir = lang === 'ar' ? 'rtl' : 'ltr';
			this._doc.documentElement.dir = dir;
			localStorage.setItem('dir', dir);

			setTimeout(() => {
				void this._initCookiesBanner();
			}, 1200);
		});
	}

	private async _initCookiesBanner() {
		if (!isPlatformBrowser(this._platformId as never)) {
			return;
		}

		if (this._ccService.isOpen()) {
			this._ccService.destroy();
		}

		const message = await lastValueFrom(this._translateService.get(this._t.landing.cookieBanner.message));
		const privacyLink = await lastValueFrom(this._translateService.get(this._t.landing.cookieBanner.privacyLink));
		const accept = await lastValueFrom(this._translateService.get(this._t.landing.cookieBanner.accept));

		const hasAccepted = localStorage.getItem('gdpr-yes');
		if (!hasAccepted) {
			const lang = `${this._translateService.currentLang || this._translateService.defaultLang}` as LangType;

			this._ccService.init({
				cookie: {
					domain: environment.mainUrl // or 'your.domain.com' // it is mandatory to set a domain, for cookies to work properly (see https://goo.gl/S2Hy2A)
				},
				palette: {
					popup: {
						background: '#130524'
					},
					button: {
						background: '#9529ff'
					}
				},
				theme: 'edgeless',
				type: 'info',
				revokable: false,
				position: 'bottom-right',
				content: {
					message,
					dismiss: accept,
					link: privacyLink,
					href: `${environment.mainUrl}${routes(lang).landing.privacy}`,
					policy: 'Cookie Policy'
				}
			});
			// this._ccService.open();

			const popCloseSub = this._ccService.statusChange$.subscribe(() => {
				localStorage.setItem('gdpr-yes', 'gdpr-yes');
				popCloseSub.unsubscribe();
			});
		}
	}
}
