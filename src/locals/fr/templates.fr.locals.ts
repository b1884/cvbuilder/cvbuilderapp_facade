export default {
	meta: {
		title: 'Modèles de CV simples et modernes',
		description:
			'Voici les modèles de CV utilisés par CV Template, un site pour faire un CV en ligne. Choisissez un modèle de CV simple et moderne.'
	},
	title: 'Modèles de CV simples et modernes',
	description:
		'Chaque modèle de CV est conçu par des experts et respecte les bonnes pratiques que les responsables du recrutement exigent. Démarquez-vous et soyez embauché plus rapidement grâce à des modèles de CV simples, modernes et séduisants.',
	choseThisModel: '{{usersNumber}}+ utilisateurs ont choisi ce modèle',
	useThisModel: 'Utiliser ce modèle'
};
