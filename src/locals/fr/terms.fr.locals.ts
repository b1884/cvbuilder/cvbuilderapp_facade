export default {
	meta: {
		title: 'Conditions Générales d’utilisation',
		description:
			'Découvrez les conditions générales d’utilisation de CV Template, un site pour faire un CV en ligne à travers un modèle simple et moderne.'
	},
	terms: `
<article class="terms-and-conditions privacy-policy container flex flex-col flex-wrap justify-start mdCustom:justify-around items-start mb-8">
    <h1>Conditions Générales d’utilisation</h1>
    <p>
        <a href="https://cv-template.co" target="_blank">Cv-template.co</a> est mis à votre disposition avec les conditions générales suivantes. Pour utiliser Cv-template.co, vous approuvez toutes les conditions citées ci-dessous.
        <br>
        Ces conditions peuvent être l’objet d’une mise à jour.
    </p>
    <h2>Article 1. Utilisation de Cv-template.co</h2>
    <p>
        <ol class='article-number-one'>
          <li>Cv-template.co vous permet - en échange d’un paiement - de créer, modifier et télécharger un ou plusieurs CVs.</li>
          <li>Une fois que vous avez activé votre compte, vous pouvez vous connecter et utiliser notre service directement.</li>
          <li>Vous devez limiter l'accès à votre compte par le biais d'un login et d'un mot de passe. En effet, il est de votre responsabilité de maintenir la confidentialité de votre compte. Vous avez le contrôle total sur toutes les activités liées à votre compte, sauf si vous nous avez informé qu'un tiers est au courant de votre mot de passe.</li>
          <li>Pour bénéficier de notre service, vous nous donnez accès à vos données personnelles. Consultez la politique de confidentialité pour plus de détails.</li>
        </ol>
    </p>
    <h2>Article 2. Conditions d'utilisation</h2>
    <p>
        <ol class='article-number-two'>
          <li>Les comptes sont créés à titre individuel. Donc, ils ne sont pas transmissibles.</li>
          <li>Il est interdit d'utiliser Cv-template.co à des fins illégales, y inclus la sauvegarde ou le partage des informations diffamatoires ou racistes et l'envoi des messages non-sollicités.</li>
          <li>Dans le cas où nous constatons que vous ne respectez pas les conditions citées ci-dessus, ou si nous recevons une plainte à ce propos, nous avons le droit de supprimer votre compte.</li>
          <li>La société Cv-template.co se réserve le droit de porter plainte à tout moment si des infractions de votre part sont constatées. Elle peut aussi vous demander des indemnités pour les dommages causés par la violation des conditions d'utilisation.</li>
          <li>Vous êtes 100% responable pour toutes les activités que vous faites en ligne avec Cv-template.co. Ces activités ne se limitent pas à la création de votre CV.</li>
        </ol>
    </p>
    <h2>Article 3. Disponibilité et maintenance</h2>
    <p>
        <ol class='article-number-three'>
          <li>La société Cv-template.co essaie de son mieux de maintenir un service permanent, mais ne peut pas garantir que notre service soit toujours disponible.</li>
          <li>La société Cv-template.co entretient le site de façon récurrente. L'entretien peut avoir lieu à tout moment et peut aussi provoquer une restriction d'accès. L'entretien sera annoncé sur la page principale du site.</li>
          <li>La société Cv-template.co peut mettre à jour les fonctionnalités du site. Vos feedbacks sont les bienvenus, mais la société Cv-template.co décidera seule quelles modifications seront exécutées.</li>
        </ol>
    </p>
    <h2>Article 4. Propriété intellectuelle</h2>
    <p>
        <ol class='article-number-four'>
          <li>Cv-template.co et les modèles de CV qui lui sont associés sont la propriété de la société Cv-template.co et de ses fournisseurs de contenu. Toute utilisation sans l'autorisation écrite de Cv-template.co, est strictement interdite, à l'exception des cas qui sont autorisés par la loi.</li>
          <li>Les informations enregistrées par vous à travers notre site, comme le contenu de votre CV ou votre photo d'identité, sont votre propriété. La société Cv-template.co a un droit limité dans le traitement de ces informations, qui est expliqué dans notre politique de confidentialité.</li>
          <li>Vous pouvez à tout moment révoquer votre droit d'utilisateur par la suppression de vos informations et/ou la résiliation de votre contrat.</li>
          <li>Vous pouvez modifier à tout moment vos informations enregistrées ou publiées à travers Cv-template.co.</li>
          <li>Si vous envoyez une information à la société Cv-template.co, par exemple des des suggestions de modification, vous nous donnez un droit illimité d'utilisation de ces informations. Ceci ne s'applique pas aux informations que vous mentionnez explicitement comme étant confidentielles.</li>
          <li>Tout au long de la durée de votre abonnement, vous avez le droit de générer des CVs avec Cv-template.co. Après la résiliation de votre abonnement, vous n'êtes plus en droit de générer ces documents.</li>
        </ol>
    </p>
    <h2>Article 5. Le paiement du service Cv-template.co</h2>
    <p>
        <ol class='article-number-five'>
          <li>Un paiement est nécesssaire pour le téléchargement d’un CV créé avec Cv-template.co</li>
          <li>L’achat d’un abonnement est obligatoire pour pouvoir utiliser notre service de façon illimité. Les frais d'abonnement concernent 7 jours d’utilisation et seront payés quand vous cliquez sur le bouton “payer”. Le paiement est effectué une seule fois. Il n’y a pas de renouvellement automatique.</li>
          <li>Vous pouvez consulter sur notre site les tarifs actuels pour l’abonnement à notre service.</li>
        </ol>
    </p>
    <h2>Article 6. Résiliation</h2>
    <p>
        <ol class='article-number-six'>
          <li>Puisque votre C.V. est livré immédiatement en ligne, à votre demande, votre droit de rétractation légal ne sera plus applicable.</li>
          <li>En souscrivant un abonnement, vous n’avez pas le droit à une rétractation. Si vous n’êtes pas satisfait de l’abonnement, vous pouvez résilier le contrat ou supprimer le compte, mais aucun remboursement vous sera dû à la résiliation.</li>
        </ol>
    </p>
    <h2>Article 7. Modalités de paiement</h2>
    <p>
        <ol class='article-number-seven'>
          <li>Le paiement est possible par une carte de crédit ou par PayPal. Votre compte sera activé après réception du paiement par la société Cv-template.co.</li>
          <li>Les frais d'abonnement dûs seront prélevés à chaque fois vous cliquez sur “Payer” dans le cas de l'abonnement de 7 jours. Par contre dans le cas de l'abonnement mensuel, il y aura un renouvellement automatique du paiement.</li>
          <li>Au cas où un retard ou un défaut de paiement est constaté, Cv-template.co est en droit de restreindre ses services, par exemple le téléchargement de votre CV.</li>
        </ol>
    </p>
    <h2>Article 8. Durée du contrat et résiliation</h2>
    <p>
        <ol class='article-number-eight'>
          <li>L'abonnement sera activé à partir du moment où vous créez votre compte.</li>
          <li>Si vous souscrivez à un abonnement, vous pouvez résilier cet abonnement à travers votre compte.</li>
          <li>En cas de résiliation de votre abonnement, les frais payés d'avance ne seront jamais remboursés à l'utilisateur.</li>
        </ol>
    </p>
    <h2>Article 9. Responsabilité</h2>
    <p>
        <ol class='article-number-nine'>
          <li>Les dispositions de responsabilité citées ici seront applicables à l'utilisation de Cv-template.co dans la mesure où aucune disposition légale s'y oppose.</li>
          <li>La responsabilité de la société Cv-template.co se limite au montant payé jusqu'à trois mois précédant le sinistre, sous réserve d’un acte intentionnel ou d’une faute lourde.</li>
          <li>La société Cv-template.co n'est pas responsable pour des dommages causés par des problèmes de gestion au niveau de l’entreprise .</li>
          <li>En cas de force majeure, la société Cv-template.co n'est pas tenue de vous indemniser pour les dommages causés. La force majeure peut être une panne d'internet ou d’éléctricité, une guerre ou un conflit national, des incendies, des inondations ou tout simplement le dysfonctionnement d'entreprise à cause de problèmes de gestion.</li>
        </ol>
    </p>
    <h2>Article 10. Modifications de conditions d’utilisation</h2>
    <p>
        <ol class='article-number-ten'>
          <li>La société Cv-template.co peut apporter des modifications sur les conditions citées ici, ainsi que les prix appliqués, et ceci dans la mesure permise par la loi.</li>
          <li>La société Cv-template.co annoncera toutes les modifications au moins 30 jours avant la prise d'effet sur notre site, afin que vous puissiez en être informé.</li>
          <li>S'il y a des modifications que vous n’approuvez pas, vous pouvez supprimer votre compte et résilier votre contrat. L'utilisation de Cv-template.co après la date de prise d'effet est considérée comme une acceptation de nouvelles conditions modifiées.</li>
        </ol>
    </p>
    <br>
    <br>
</article>
	`
};
