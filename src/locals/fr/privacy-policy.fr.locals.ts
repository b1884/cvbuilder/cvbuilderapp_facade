export default {
	meta: {
		title: 'Politique de confidentialité',
		description:
			'Découvrez la politique de confidentialité de CV Template, qui est conforme aux directives du Règlement Général sur la Protection de Données.'
	},
	policy: `
<article class="terms-and-conditions container flex flex-col flex-wrap justify-start mdCustom:justify-around items-start mb-8">
    <h1>Politique de confidentialité</h1>
    <p>
        <a href="https://cv-template.co" target="_blank">Cv-template.co</a> respecte à la lettre le Règlement Général sur la Protection de Données (RGPD) tel qu’il a été promulgué par le Parlement Européen le 27 Avril 2016 et mis en application à partir de 25 Mai 2018.
    </p>
    <p>
        Cela signifie que :
        <br>
        <ul>
            <li>Nous ne collectons pas des données sensibles ou critiques à caractère très privé.</li>
            <li>Nous limitons notre collecte de données aux données personnelles fournies par l’utilisateur et qui sont nécessaires à des fins légitimes.</li>
            <li>Nous n’utilisons pas ces données à une fin autre que la création de votre CV.</li>
            <li>Nous demandons l’autorisation (le consentement) de l'utilisateur pour placer des cookies.</li>
            <li>Nous prenons toutes les mesures de sécurité nécessaires pour protéger vos données personnelles.</li>
            <li>Nous respectons votre droit d’obtenir vos données personnelles à des fins de contrôle, de correction ou de suppression.</li>
        </ul>
    </p>
    <p>
        Dans cette politique de confidentialité, nous expliquons quelles sont les données personnelles que nous traitons, et dans quel but. Nous expliquons aussi notre politique de cookies. Du coup, nous vous conseillons de lire cette page attentivement.
    </p>
    <h2>Traitement des données personnelles</h2>
    <p>
        En utilisant Cv-template.co, vous allez nous fournir certaines de vos données personnelles. Nous stockons et traitons uniquement les données personnelles que vous donnez activement ou passivement, dans le contexte d'un service de création d’un CV que vous avez demandé ou utilisé. Donc, vous nous avez donné de cette façon le droit de traiter vos données.
    </p>
    <h2>Création & Enregistrement de CV</h2>
    <p>
        En créant un CV, vous créez automatiquement un compte dans notre plateforme. Ce compte fait partie de votre abonnement. Nous utilisons les données suivantes pour créer votre CV:
        <br>
        <ul>
            <li>Nom et adresse</li>
            <li>Numéro de téléphone</li>
            <li>Adresse de facturation</li>
            <li>Adresse e-mail</li>
            <li>Données de paiement</li>
            <li>Sexe</li>
            <li>Votre photo</li>
            <li>Date de naissance</li>
            <li>Adresse IP</li>
            <li>Les informations telles que mentionnées sur votre CV, comme par exemple votre expérience professionnelle et vos formations</li>
        </ul>
    </p>
    <p>
        Lors de l'enregistrement, nous sauvegardons dans notre base de données les données personnelles que vous avez transmises. Nous les sauvegardons déjà à partir du moment où vous commencez à créer un compte.
    </p>
    <p>
        Vous pouvez vous connecter à votre compte avec l'adresse e-mail et le mot de passe que vous nous avez indiqué. Nous sauvegardons ces données pour que vous ne deviez pas à chaque fois les saisir,  et pour que nous puissions vous contacter dans le cadre de l'exécution de l'accord, de la facturation et du paiement.
    </p>
    <p>CV Template traite vos données personnelles pour sauvegarder votre avancement, en particulier les CVs que vous avez créés, afin de les mettre à votre disposition lors d'une future utilisation de nos services. </p>
    <p>Si vous n'avez pas terminé le CV, nous pouvons vous envoyer des e-mails de rappel.</p>
    <h2>Délais de conservation de données</h2>
    <p>CV Template sauvegarde vos données personnelles aussi longtemps que nécessaire pour réaliser vos objectifs de modification ou de suppression. </p>
    <p>Ainsi, nous conservons vos données personnelles jusqu'à ce que vous demandiez de supprimer votre compte.</p>
    <p>Si vous gardez votre compte inactif pendant une longue période, CV Template peut vous contacter pour vous demander la sauvegarde ou non de vos données personnelles.</p>
    <h2>Fourniture de données à des tiers</h2>
    <p>Nous ne donnons vos données à aucune tierce partie.</p>
    <p>Des comptes de réseaux sociaux sont liés à notre site. Les administrateurs de ces services peuvent recueillir certaines de vos données personnelles.</p>
    <h2>Cookies</h2>
    <p>Les cookies sont des petits fichiers d'information qui sont sauvegardés lors d'une visite à un site Internet ou qui peuvent être lus à partir de l'appareil du visiteur.</p>
    <p>Lors de la visite à Cv-template.co, une pop-up s'affiche pour vous demander d'accepter l'installation de cookies.</p>
    <p>A partir du moment où vous cliquez “J’accepte”, vous nous donnez l'autorisation d'utiliser les cookies de Google Analytics. Mais, vous êtes toujours libre d'éliminer les cookies en utilisant un plugin approprié.</p>
    <h3>Google Analytics</h3>
    <p>Nous utilisons des cookies pour acquérir un aperçu de l'utilisation de notre site Internet, et pour améliorer cette utilisation à l'aide de cet aperçu.</p>
    <p>A cet effet, nous utilisons Google Analytics pour enregistrer la façon dont les visiteurs utilisent notre site Internet.</p>
    <p>Dans notre accord avec Google, nous n'avons pas autorisé Google à utiliser les informations pour d'autres services et finalement, les adresses IP seront rendues anonymes. Vous pourrez trouver plus d'informations sur le traitement des données dans <a href="http://www.google.com/intl/fr/policies/privacy/" target="_blank" rel="nofollow">la politique de confidentialité de Google</a>. </p>
    <p>Ces données sont conservées pendant 2 ans.</p>
    <h2>Sécurité de données</h2>
    <p>Nous prenons toutes les mesures de sécurité adéquates pour limiter les abus et l'accès non autorisé aux données personnelles.</p>

    <h2>Sites de tiers</h2>
    <p>Cette politique de confidentialité et de cookies ne s'applique pas aux sites Internet de tiers qui sont connectés à ce site Internet par le biais de liens.</p>
    <p>Nous ne pouvons pas garantir que des tiers traitent vos données personnelles de manière fiable ou sûre. </p>
    <p>Nous vous conseillons de lire la politique de confidentialité de ces sites avant d’utiliser leurs services.</p>

    <h2>Modifications de la politique de confidentialité</h2>
    <p>Nous avons le droit d'apporter des modifications à cette politique de confidentialité et de cookies. Il est conseillé de régulièrement consulter cette politique de confidentialité et de cookies pour rester informé de ces modifications.</p>
    <h2>Vos droits</h2>
    <p>
        <ul>
            <li>Obtenir des explications sur les données personnelles que nous détenons.</li>
            <li>Demander de corriger des erreurs.</li>
            <li>Demander de supprimer des données personnelles.</li>
            <li>Retirer l'autorisation ou supprimer définitivement votre compte.</li>
        </ul>
    </p>
    <p>Pour se renseigner encore sur notre politique ou pour envoyer une plainte , vous pouvez à tout moment nous contacter via les coordonnées ci-dessous et nous ferons de notre mieux pour répondre correctement à vos besoins.</p>
    <br>
    <br>
    <br>
    <br>
    <h2>Contact</h2>
    <p>
        <ul>
            <li>Adresse: CV Template, Entrepria Coworking Space, Monastir 5000, Tunisie</li>
            <li>Email: <a href='mailto:hi@cv-template.co'>hi@cv-template.co</a></li>
        </ul>
    </p>
</article>`
};
