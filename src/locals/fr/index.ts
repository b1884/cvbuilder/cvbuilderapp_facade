import common from './common.fr.locals';
import landing from './landing.fr.locals';
import auth from './auth.fr.locals';
import terms from './terms.fr.locals';
import privacy from './privacy-policy.fr.locals';
import pricing from './pricing.fr.locals';
import about from './about-us.fr.locals';
import faq from './faq.fr.locals';
import templates from './templates.fr.locals';

export default {
	landing,
	common,
	auth,
	terms,
	pricing,
	privacy,
	about,
	faq,
	templates
};
