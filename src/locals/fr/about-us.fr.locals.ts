export default {
	meta: {
		title: 'À propos de nous',
		description:
			'Découvrez la culture de l’entreprise de CV Template, un site pour faire un CV en ligne à travers un modèle simple et moderne.'
	},
	aboutUs: `
  <article class="terms-and-conditions container flex flex-col flex-wrap justify-start mdCustom:justify-around items-start mb-8">
    <h1>À propos de nous</h1>
    <p>Dans Cv-template.co, nous croyons que la création d'un CV professionnel est une étape nécessaire pour augmenter ses chances d'être embauché. En tant que chercheur d’emploi, votre carrière commence toujours par la bonne première impression que vous allez donner à travers un CV bien écrit.</p>
    <p>Pour cet objectif, nous avons créé un site web simple à utiliser, qui vous permet non seulement d’écrire un CV séduisant mais de le télécharger en format PDF. Ce CV respecte à la lettre les standards exigés par les recruteurs aujourd'hui car nous vous offrons des conseils clairs sur la bonne manière de remplir toutes les sections.</p>
    <p>Chaque jour, nous travaillons pour améliorer davantage l’expérience utilisateur sur notre site en proposant de nouveaux modèles de CV et de nouvelles options d’utilisation.</p>
    <p>Plusieurs CV ont été créés à travers notre site et ont permis à nos utilisateurs de décrocher des jobs au sein d'entreprises réputées.</p>
     <br>
  </article>
	`
};
