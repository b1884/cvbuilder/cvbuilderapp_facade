export default {
	meta: {
		title: 'Faire un CV en ligne par CV Template',
		description:
			'Sachez faire un CV en ligne par la découverte de toutes les bonnes pratiques nécessaires et offertes par CV Template, un site pour créer un CV en ligne.'
	},
	title: 'Faire un CV en ligne par CV Template',
	faq: {
		question1: {
			q: 'Quelles sont les bases pour faire un CV?',
			a: `
        <p>Élément indispensable dans le processus de recherche d’emploi, le curriculum vitae (CV) renseigne l’employeur sur votre profil, vos compétences et votre parcours académique et professionnel. Et face aux candidats concurrents qui postulent au même emploi, il est important d’avoir un CV original et séduisant pour se différencier des autres et attirer l’attention du recruteur. </p>
        <p>L’objectif ultime d’un CV est de mettre en valeur vos qualités et vos compétences, en particulier celles les plus pertinentes pour le poste offert. En effet, le curriculum vitae doit montrer la compatibilité parfaite entre vos compétences et les besoins de l’entreprise qui recrute. Ce score de compatibilité est calculé aujourd’hui par les systèmes ATS (Applicant Tracking Systems) qui sont les logiciels qui sélectionnent les candidats à interviewer. </p>
        <p>Il est quasiment impossible de décrocher un job sans avoir un CV bien rédigé ! Même si quelqu’un vous recommande à une entreprise, cette dernière vous demandera certainement votre CV pour avoir une idée de votre parcours. Créer un CV professionnel est donc une condition indispensable ! Pensez toujours à mettre votre CV à jour afin qu’il reflète les dernières expériences professionnelles et formations que vous avez réalisées.</p>
        <p>Pour faire un bon CV, n’utilisez jamais un simple éditeur de texte comme Microsoft Word. Bien que ce soit une méthode très courante pour faire un CV, elle est loin d'être la meilleure.</p>
        <p>En utilisant un éditeur de texte, vous serez obligé de passer des heures à régler et affiner la mise en page de votre CV. Et le résultat final est généralement un CV mal structuré qui donne une impression d’amateurisme. </p>
        <p>Au lieu d'utiliser un éditeur de texte, nous vous recommandons de choisir un générateur de CV, comme notre <a href='https://cv-template.co'>CV Designer</a>.</p>
        <p>Dans Cv-template.co, nous avons implémenté le meilleur modèle de CV pour vous aider à faire un CV moderne qui vous garantit un entretien d’embauche. Nous avons conçu ce modèle après l’analyse de plusieurs CV professionnels. Il inclut ainsi toutes les bonnes pratiques que les recruteurs cherchent.</p>
      `
		},
		question2: {
			q: 'Quels sont les éléments à inclure pour faire un CV?',
			a: `
			<h3>Le prénom, le nom et les coordonnées de contact</h3>
			<p>Le prénom, le nom, le numéro de téléphone et l’adresse email font partie des informations essentielles dans chaque CV. On peut ensuite ajouter une photo, de mentionner sa date de naissance, son permis de conduire, son état matrimonial, sa nationalité ou encore ses comptes sociaux.</p>

			<h3>Le résumé du CV</h3>
			<p>Ajouter un petit texte pour se présenter est une bonne pratique pour valoriser votre CV. La biographie ou le résumé d’un CV consiste à résumer en deux à trois phrases votre parcours et vos compétences et mentionner votre objectif.</p>
			<p>Cette introduction doit être accrocheuse, afin qu’elle puisse motiver les recruteurs à lire votre CV dans son intégralité ! Mieux vaut donc privilégier une biographie courte et qui répond parfaitement au profil indiqué dans l’offre de l’emploi.</p>

			<h3>L’expérience professionnelle</h3>
			<p>Dans cette rubrique, expliquez aux recruteurs vos expériences professionnelles, qu’il s’agisse de vos stages ou de vos emplois précédents. De cette manière, les recruteurs pourront avoir une idée très précise sur votre parcours professionnel, et comprendre les compétences que vous maitrisez.</p>
			<p>Pensez donc à lister des réalisations et à donner des exemples concrets et chiffrés, comme l’atteinte d’un objectif extraordinaire de ventes mensuelles, l’obtention d’un prix, le management d’un projet spécifique dans une durée record.</p>

      <h3>La formation</h3>
			<p>Vos études, vos diplômes et vos certificats de formation sont des éléments absolument essentiels dans la rédaction de votre CV. En effet, ces informations permettent au recruteur de savoir comment vous avez eu cette formation, votre niveau de qualification et dans quel domaine vous êtes compétents. Le niveau de qualification et d’expertise a un effet sur le niveau de rémunération.</p>

			<h3>Les compétences</h3>
			<p>Indiquer vos compétences et votre savoir-faire fait partie des éléments incontournables à mettre dans un CV. Il peut s’agir des compétences douces (soft skills) comme le leadership et la capacité de travailler en équipe ou des compétences techniques (Hard Skills) comme Excel ou JavaScript.</p>
			<p>Dans tous les cas, ces compétences doivent montrer aux recruteurs que vous êtes capables de bien exécuter des missions et des tâches particulières souvent demandées par l’entreprise. </p>

			<h3>Les langues étrangères</h3>
			<p>Les langues sont un atout considérable dans la recherche d’emploi. Elles doivent alors apparaître dans le CV. L’anglais est fortement demandé par le marché de travail actuel. Un niveau B2 en anglais augmente vos chances d' accéder à un entretien.</p>
			<p>Les autres langues étrangères peuvent être un sérieux atout pour votre candidature et permettre de se démarquer des autres candidats. Pensez à indiquer votre niveau, en toute honnêteté et sans exagérer, pour chacune d’entre elles !</p>

			<h3>Les centres d’intérêts </h3>
			<p>Si vous avez une ou plusieurs passions, vous pouvez les mentionner sur votre CV. Cela permettra aux recruteurs de bien saisir votre type de personnalité. Cette section vous aide à vous démarquer des autres candidats.</p>
			<p>Nous vous recommandons d’indiquer quatre passe-temps au maximum pour ne pas trop surcharger votre CV. Les hobbies que vous mentionnez doivent présenter un intérêt très particulier. Ils doivent être aussi un véritable atout pour le poste recherché.</p>

			<h3>Les activités extra-professionnelles</h3>
			<p>Il n’y a pas que les expériences professionnelles qui sont importantes dans un CV. Si vous êtes membre d’une association sportive ou un club de faculté, dites-le !</p>
			<p>Tout comme vos expériences professionnelles, ces activités mettent en avant vos compétences et permettent d’en apprendre plus sur votre personnalité.</p>
     `
		},
		question3: {
			q: 'Quelles sont les règles à respecter pour faire un CV?',
			a: `
      <p>Puisque le marché de travail actuel est concurrentiel, envoyer un CV professionnel et moderne en format pdf est obligatoire. Ainsi, nous vous conseillons d'utiliser <a href='https://cv-template.co'>Cv-template.co</a>. Il suffit juste de choisir un modèle et remplir un formulaire pour pouvoir générer votre CV en pdf.</p>
			<p>Voici les règles à suivre pour écrire un bon CV :</p>

			<h3>Organisez votre CV</h3>
			<p>Créer différentes rubriques dans un CV permet de l’organiser et de rendre sa lecture plus agréable. Il est donc important de ne pas inclure toutes les informations dans un seul bloc.</p>

			<h3>Expliquez les formations et les expériences</h3>
			<p>Lors de la rédaction de votre CV, essayez toujours d’aller à l’essentiel en expliquant vos expériences professionnelles. Donnez des exemples concrets et chiffrés et utilisez des mots simples et évitez les acronymes sauf s’ils sont vraiment incontournables.</p>

			<h3>Évitez de surcharger votre CV</h3>
			<p>Faites le tri dans les informations que vous voulez mentionner afin de vous concentrer uniquement sur celles qui présentent un véritable intérêt pour le poste recherché. Nous vous conseillons de ne pas dépasser une page A4. Au-delà, les recruteurs peuvent abandonner votre CV dans son intégralité.</p>

      <h3>Écrivez pour les systèmes ATS (Applicant Tracking Systems</h3>
			<p>Les ATS sont les logiciels qui sélectionnent les CVs. Pour cela, ils évaluent la compatibilité de votre CV avec l’annonce de travail. Du coup, votre CV doit contenir seulement les compétences les plus pertinentes pour le poste. Donc, analysez bien l’annonce de travail et essayez de rédiger un CV personnalisé qui répond parfaitement au besoin de l’entreprise.</p>

			<h3>Éliminez les fautes de tout type</h3>
			<p>L’orthographe doit être irréprochable. Ainsi, elle donne une meilleure image de vous aux recruteurs. On doit relire son CV plusieurs fois et demander même l’avis d’un proche pour valider le contenu;</p>

			<h3>Soignez la mise en page</h3>
			<p>La forme d’un CV compte autant que le fond ! Il est donc important de soigner la mise en page de son CV en créant des titres visibles, en ajustant le texte et en utilisant une police de caractère professionnelle et lisible pour motiver les recruteurs à se plonger dans la lecture de votre CV.</p>

			<h3>Mettez votre CV régulièrement à jour</h3>
			<p>Assurez-vous de toujours mettre à jour votre CV avant de l’envoyer aux recruteurs, pour qu’il puisse refléter correctement votre situation actuelle.</p>

			<h3>Évitez les trous dans votre CV</h3>
			<p>Évitez à tout prix de laisser des périodes d’inactivité pendant lesquelles vous n’avez pas suivi une formation ou réalisé une expérience professionnelle, parce que cela peut donner une mauvaise image de vous aux recruteurs. Si vous avez pris une année sabbatique pour vous occuper de votre famille ou faire un voyage, dites-le!</p>

			<h3>Mettez une photo professionnelle</h3>
			<p>Si vous décidez d’ajouter une photo sur votre CV, n’utilisez pas celle que vous avez prise lors de vos dernières vacances ! La photo dans un CV doit être professionnelle. Il vaut mieux sourire dans la photo. Cela vous rend plus appréciable.</p>

			<h3>N'exagérez pas et soyez honnêtes</h3>
			<p>Lors de la rédaction de votre CV, évitez à tout prix les tournures du type “Je suis le candidat qu’il vous cherchez” ou “J’aime sortir de ma zone de confort”. Ce sont des formules qui exaspèrent les recruteurs. Un autre conseil fondamental, il ne faut pas mentir. Dans l’entretien, on peut tout dévoiler.</p>

			<h3>Ne jamais indiquer vos prétentions salariales</h3>
			<p>le CV n’est pas le lieu pour indiquer le salaire que vous voulez avoir. Vous aurez tout le temps nécessaire d’aborder ce sujet lors de l’entretien d’embauche !</p>
     `
		}
	}
};
