export default {
	login: {
		meta: {
			title: 'Connectez-vous',
			description:
				'Utilisez CV Template (Curriculum Vitae Designer) pour faire un CV en ligne via un modèle de CV simple et moderne.',
			keywords: 'CV Designer, Curriculum Vitae Designer, Faire un CV, Modèle de CV'
		},
		title: 'Connexion',
		description: 'Votre e-mail',
		label: 'E-mail',
		question: "Vous n'avez pas encore créé votre CV ? ",
		ctaSignup: 'Créez-en un maintenant!',
		errors: {
			emailExists: "L'e-mail que vous avez entré n'existe pas!"
		}
	},
	emailSent: {
		title: 'Vérifiez votre boîte de réception',
		description:
			'Nous avons envoyé un lien de confirmation à <b>{{email}}</b>. Cliquez sur le lien et vous serez connecté.'
	},
	signup: {
		meta: {
			title: 'Créez votre meilleur CV',
			description:
				'Utilisez CV Template pour créer votre meilleur CV en ligne via un modèle de CV simple et moderne.',
			keywords: 'CV Designer, Modèle de CV, CV en ligne'
		},
		title: 'Faisons connaissance!',
		description1:
			"Pour être contacté par les recruteurs, vous devez fournir vos informations d'identification à l'aide de:",
		description2: 'Ou bien, entrez votre nom et votre adresse e-mail ci-dessous.',
		fields: {
			name: {
				label: 'Prénom'
			},
			lastName: {
				label: 'Nom'
			},
			email: {
				label: 'E-mail'
			}
		},
		errors: {
			emailExists: "L'e-mail que vous avez entré existe déjà"
		},
		question: 'Vous avez déjà créé votre CV ?',
		ctaSignin: 'Connectez-vous!'
	}
};
