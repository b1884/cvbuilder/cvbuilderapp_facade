export default {
	meta: {
		title: 'Faire un CV en ligne via un modèle de CV simple',
		description:
			'CV Designer vous aide à faire un CV en ligne (Curriculum Vitae) via un modèle de CV simple à télécharger. L’idéal pour avoir un entretien d’embauche!',
		keywords: 'CV Designer, Curriculum Vitae, Faire un CV, Modèle de CV, CV en ligne'
	},
	hero: {
		title: 'Créez un CV qui vous garantit un entretien',
		description:
			'Choisissez un modèle professionnel, remplissez un formulaire et téléchargez un CV qui plaît aux recruteurs.',
		liveCounter: "{{cvs}} CV ont été créés aujourd'hui."
	},
	inDepthDescription: {
		title: 'Créez le CV idéal pour avoir un job',
		description:
			'Décrocher un job vous semble difficile. Nous allons vous simplifier les choses.<br><br>' +
			'En analysant les CVs les plus pros, nous avons conçu le CV designer le plus adapté au marché de travail actuel. <br><br>' +
			'Choisissez un modèle, remplissez un formulaire en suivant nos consignes et téléchargez un CV qui séduit les recruteurs.'
	},
	icon1: {
		title: 'C’est à la fois un CV designer et un coach',
		description:
			"En remplissant les sections du formulaire, suivez nos consignes et racontez une histoire professionnelle qui suscitera l'intérêt des recruteurs."
	},
	icon2: {
		title: 'Faire un CV en ligne n’a jamais été aussi simple',
		description:
			'Notre CV designer simplifie la rédaction de votre CV. Il vous suffit juste de remplir un formulaire pour générer un CV professionnel et séduisant.'
	},
	icon3: {
		title: 'Augmentez vos chances avec un CV Designer testé',
		description:
			'Pour augmenter vos chances sur ce marché d’emploi concurrentiel, nous avons validé notre Curriculum Vitae Designer auprès des experts en GRH.'
	},
	templates: {
		title: 'Des modèles de CV simples, modernes et élégants',
		description:
			'Séduisez les employeurs en utilisant un modèle de CV très élégant et validé par les professionnels du recrutement.' +
			'<br> <br>' +
			'Téléchargez-le en PDF ou générez un lien à partager partout.'
	},
	reviews: {
		title: 'Vous serez 100% satisfait!',
		rev1: {
			name: 'Adam',
			job: 'Développeur Fullstack',
			review: 'CV Template m’a appris à écrire pour les Applicant Tracking Systems. Ce sont les logiciels qui sélectionnent les CVs. En être conscient est essentiel.'
		},
		rev2: {
			name: 'Marie',
			job: 'Aide-soignante',
			review: 'L’avantage de CV Template par rapport aux autres, c’est qu’il offre non seulement un template mais aussi des conseils clairs pour bien rédiger votre CV.'
		},
		rev3: {
			name: 'Elias',
			job: 'Sales Rep.',
			review: 'Grâce à CV Template, j’ai eu mon premier entretien et j’ai pu décrocher le job d’un sales rep dans une startup. J’ai partagé alors le site avec mes amis.'
		}
	},
	lastCTA: {
		title: 'Rejoignez plus de <span class="all-users-home">&nbsp;12,000&nbsp;</span> utilisateurs satisfaits!',
		description: 'Essayez notre CV designer maintenant et faites votre premier pas vers la carrière de vos rêves.'
	},
	footer: {
		copyrights: 'Mettons fin au chômage! © <a href="{{website}}">Cv-template.co</a> {{year}} 🤘',
		section1: {
			title: 'Gardons le contact sur'
		},
		section3: {
			title: 'Service client',
			faq: 'FAQ',
			contactUs: 'Contactez-nous',
			privacy: 'Politique de confidentialité',
			terms: 'Conditions Générales',
			about: 'À propos'
		},
		section2: {
			title: 'Produit',
			pricing: 'Tarifs',
			models: 'Modéles',
			createCv: 'Créer un CV',
			blog: 'Blog'
		}
	},
	cookieBanner: {
		message: 'Pouvons-nous placer des cookies ? Consultez notre',
		privacyLink: 'politique de cookies.',
		accept: "J'accepte"
	}
};
