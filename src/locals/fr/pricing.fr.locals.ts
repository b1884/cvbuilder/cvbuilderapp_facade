export default {
	meta: {
		title: 'Tarifs',
		description:
			'Découvrez les prix d’abonnement appliqués par CV Template, un site pour faire un CV en ligne à travers un modèle simple et moderne.'
	},
	plans: {
		savingAmount: "Économisez jusqu'à 20 %.",
		bestPlan: 'Meilleur plan!',
		weekly: {
			feature1: 'Téléchargements PDF Illimités',
			feature2: 'Partage illimité des liens des CVs',
			feature3: 'Création et modification illimitées des CVs',
			feature4: 'Abonnement <span class="all-users-home px-1">7 jours</span>',
			feature5: 'Pas de renouvellement automatique de paiement'
		},
		monthly: {
			feature1: 'Téléchargements PDF Illimités',
			feature2: 'Partage illimité des liens des CVs',
			feature3: 'Création et modification illimitées des CVs',
			feature4: 'Abonnement <span class="all-users-home px-1">28 jours</span>',
			feature5: 'Renouvellement automatique chaque 4 semaines'
		},
		free: {
			feature1: "Création d'un seul CV",
			feature2: "Partage de liens d'un seul CV",
			feature3: 'Pas de téléchargements'
		}
	},
	title: 'Tarifs',
	pricing: {
		question1: {
			q: 'Comment fonctionne l’abonnement?',
			a: "Si vous achetez un abonnement, vous pouvez modifier votre CV, créer plus qu'un CV et télécharger vos CV pendant la durée de l'abonnement. Une fois cette période terminée, des fonctionnalités comme le téléchargement de CV ne sont plus autorisées. Pour y bénéficier de nouveau, vous devez renouveler l’achat de votre abonnement."
		},
		question2: {
			q: 'Ai-je un accès complet avec cet abonnement?',
			a: 'Oui ! L’abonnement vous donne un accès à toutes les fonctionnalités, y compris le téléchargement de CV.'
		},
		question3: {
			q: 'Comment s’effectue le paiement?',
			a: 'Le paiement peut être effectué par carte de crédit ou par Paypal. Notre plateforme de paiement est hautement sécurisée.'
		},
		question4: {
			q: 'Est-ce qu’il y a un renouvellement automatique de paiement?',
			a: "Non. Il n’y a pas de renouvellement automatique de paiement dans le cas de l'abonnement 7 jours. Le paiement se fait uniquement au moment où vous cliquez sur le bouton “Payer” et couvre 7 jours d’utilisation. Pour l'abonnement mensuel, il y un retrait automatique chaque 4 semaines que vous pouvez stopper à tout moment."
		}
	}
};
