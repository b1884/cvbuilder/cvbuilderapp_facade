export default {
	misc: {
		login: 'Connexion',
		signup: 'Inscription',
		createCv: 'Créer un CV',
		createBestCV: 'Créer mon meilleur CV',
		selectTemplate: 'Sélectionner un modèle',
		blog: 'Blog',
		faq: 'FAQ',
		myAccount: 'Mon compte',
		languages: 'Langues: '
	},
	buttons: {
		french: 'Français',
		english: 'Anglais',
		arabic: 'Arabe',
		continue: 'Continuer',
		back: 'Retour'
	}
};
