export default {
	login: {
		meta: {
			title: 'Login',
			description:
				'Use CV Template (Curriculum Vitae Designer) to create a CV online via a simple and modern CV template.',
			keywords: 'Curriculum Vitae Designer, Create CV online, CV template'
		},
		title: 'Login',
		description: 'Your E-mail',
		label: 'E-mail',
		question: "Haven't created your CV yet? ",
		ctaSignup: 'Create a CV now!',
		errors: {
			emailExists: 'The e-mail you entered does not exist!'
		}
	},
	emailSent: {
		title: 'Check your inbox',
		description:
			'We have sent you a confirmation link to <b>{{email}}</b>. Click on the link, and you will be connected.'
	},
	signup: {
		meta: {
			title: 'Create your best CV',
			description:
				'Use CV Template (Curriculum Vitae Designer) to create your best CV online via a simple CV template.',
			keywords: 'Curriculum Vitae Designer, Create CV online, CV template'
		},
		title: "Let's begin!",
		description1: 'To be contacted by recruiters, you must provide your identification information using:',
		description2: 'Or, enter your name and email address below.',
		fields: {
			name: {
				label: 'First name'
			},
			lastName: {
				label: 'Last name'
			},
			email: {
				label: 'E-mail'
			}
		},
		errors: {
			emailExists: 'The email you entered already exists'
		},
		question: 'Have you already created your CV?',
		ctaSignin: 'Log in!'
	}
};
