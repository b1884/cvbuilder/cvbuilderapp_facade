export default {
	meta: {
		title: 'Create a curriculum vitae via a CV maker',
		description:
			'CV Template helps you create a curriculum vitae. Try the best CV maker to build a resume that seduces recruiters and gets you a job interview.',
		keywords: 'CV Template, Curriculum Vitae, CV Maker'
	},
	hero: {
		title: 'Create a CV that gets you a job interview.',
		description:
			'Choose a CV template. Fill out a form. Download a CV that follows the best practices requested by recruiters.',
		liveCounter: '{{cvs}} CVs made today.'
	},
	inDepthDescription: {
		title: 'Create a professional CV that seduces recruiters',
		description:
			'Getting a job seems like a difficult task. We are here to make it easy for you.<br><br>' +
			'After analyzing tons of professional CVs, we created the most suitable CV maker for the job market <br><br>' +
			'Choose a CV template, fill out a form using our writing tips and download an attractive CV that guarantees you a job interview.'
	},
	icon1: {
		title: 'It is both a CV Maker and a coach',
		description:
			'Our CV Maker meets all the best practices of CV writing. When filling out the sections, follow our guidelines and tell a story that attracts recruiters.'
	},
	icon2: {
		title: 'Creating a CV online has never been easier',
		description:
			'Writing your CV is very easy. All you have to do is fill out a form. And in less than 15 minutes, you will generate a professional and attractive CV.'
	},
	icon3: {
		title: 'Increase your chances with a well-tested CV Template',
		description:
			'To increase your chances in this very competitive job market, we validated our CV templates with HR experts. You will be using the best models in the job market!'
	},
	templates: {
		title: 'CV Template: Simple, Modern & Elegant',
		description:
			'Convince employers with a stylish CV template validated by recruiting professionals. ' +
			'<br> <br>' +
			'Download it as a PDF or generate a link to share anywhere.'
	},
	reviews: {
		title: 'You will be 100% satisfied!',
		rev1: {
			name: 'Adam',
			job: 'Full-stack developer',
			review: 'CV Template taught me how to write the perfect CV for Applicant Tracking Systems. These are the softwares that select CVs today. Being aware of how they work is essential.'
		},
		rev2: {
			name: 'Mary',
			job: 'Nurse',
			review: 'The advantage of CV Template over other solutions is that it offers not only a good CV template but also clear tips on how to write a good CV. It is the perfect solution.'
		},
		rev3: {
			name: 'Elias',
			job: 'Sales Rep.',
			review: 'Thanks to CV Template, I had my first job interview and was able to land a sales rep job in a startup. I shared the site with all my friends who are looking for a job.'
		}
	},
	lastCTA: {
		title: 'Join over <span class="all-users-home">&nbsp;12,000&nbsp;</span> satisfied users!',
		description: 'Try our CV Maker now and take your first step towards the career of your dreams.'
	},
	footer: {
		copyrights: 'Let\'s end unemployment! © <a href="{{website}}">Cv-template.co</a> {{year}} 🤘',
		section1: {
			title: 'Keep in touch'
		},
		section3: {
			title: 'Support',
			faq: 'FAQ',
			contactUs: 'Contact us',
			privacy: 'Privacy',
			terms: 'Terms of services',
			about: 'About us'
		},
		section2: {
			title: 'Product',
			pricing: 'Pricing',
			models: 'Templates',
			createCv: 'Create a CV',
			blog: 'Blog'
		}
	},
	cookieBanner: {
		message: 'Can we set cookies? Check out our',
		privacyLink: 'cookies policy.',
		accept: 'I agree'
	}
};
