export default {
	meta: {
		title: 'How to make a CV using the best CV Maker',
		description:
			'Learn how to make a CV using the best CV maker on the market. Build a resume that can seduce recruiters and get you a job interview. '
	},
	title: 'How to make a CV using CV Template',
	faq: {
		question1: {
			q: 'What is a CV?',
			a: `
        <p>An essential element in the job search process is the curriculum vitae (CV). It’s the document that informs the employer about your profile, your skills, and your academic and professional background. And when faced with competing candidates applying for the same job, it is important to have an attractive and original CV to stand out from the crowd and attract the recruiter's attention.  </p>
        <p>The ultimate objective of a CV is to highlight your qualities and skills, especially those most relevant to the position offered. Indeed, the resume must show the perfect compatibility between your skills and the needs of the company that is recruiting.  This compatibility score is calculated today by ATS (Applicant Tracking Systems), the software that selects candidates to interview. </p>
        <p>It is almost impossible to get a job without a well-written CV! Even if someone recommends you to a recruiter, for example, the recruiter will certainly ask for your CV to get an idea of your background. Creating a professional CV is therefore absolutely essential! Also, remember to keep your CV up to date so that it always contains your latest professional experiences and certifications. It is therefore important to always read it again before sending it to a recruiter, to be sure that you have not forgotten anything.</p>
        <p>To contact the recruiter and show him that you are the ideal candidate for the position he is offering, you need a CV. This document will allow the recruiter to know in one or two pages your qualities, experiences, and skills.</p>
        <p>Since today's job market is competitive, sending a professional and modern resume in pdf format is mandatory. Thus, we recommend you to use our <a href='https://cv-template.co'>CV Designer</a>. Just choose a template and fill out a form to generate your CV in pdf. </p>
      `
		},
		question2: {
			q: 'What to put in a CV?',
			a: `
			<h3>Name & contact information</h3>
			<p>The name, city, phone number, and email address are essential information for any CV. You can then choose whether or not to add a photo, mention your date of birth, driver's license, marital status, nationality, or social accounts. </p>

			<h3>Summary</h3>
			<p>Adding a short text to introduce yourself is a good practice to enhance your CV. The biography or summary of a CV consists of summarizing your background and skills in a few sentences and mentioning your objective in order to introduce yourself to employers.</p>
			<p>This introduction must be catchy, in order to make recruiters want to know more about you and read your entire CV! It is, therefore, best to keep the introduction short and to match the profile sought by the employer.</p>

			<h3>Work experience</h3>
			<p>In this section, tell the recruiters about your professional experience, whether it is your previous internships or jobs. This way, recruiters can get a better idea of your professional background, and thus learn more about your skills and what you are capable of accomplishing.</p>
			<p>Think of giving concrete and quantified examples, such as the publication of a study article, obtaining an award, or the management of a specific project.</p>

      <h3>Education</h3>
			<p>Your studies, diplomas, and training certificates are absolutely essential elements in the writing of your CV. Indeed, this information allows the recruiter to know where and how you were trained, your level of qualification, and in which field(s) you are competent. The level of qualification can also have an impact on your salary, for example.</p>

			<h3>Skills</h3>
			<p>Indicating your skills and therefore your know-how is an essential part of any CV. These can be soft skills such as leadership and the ability to work in a team or hard skills such as Excel or JavaScript.</p>
			<p>In any case, these skills must show recruiters that you are capable of carrying out the specific tasks and missions requested by the company. </p>

			<h3>Foreign languages</h3>
			<p>Languages are an important asset in the job search and must therefore appear in the CV. English is in high demand in the current job market. A B2 level in English increases your chances of getting an interview. </p>
			<p>Other foreign languages can be a serious asset for your application and allow you to stand out from other candidates. Remember to indicate your level, honestly, for each of them! </p>

			<h3>Interests </h3>
			<p>If you have one or more passions, you can mention them on your CV. This will allow recruiters to know a little more about your personality and set you apart from other candidates.</p>
			<p>We recommend that you list a maximum of four hobbies, so as not to overload your CV. The ones you choose should be of particular interest and a real asset for the position you are applying for. </p>

			<h3>Extra-professional activities</h3>
			<p>Professional experience is not the only thing that counts in a CV. If you are a member of a sports association or a faculty club, say so! </p>
			<p>Just like your professional experience, these activities highlight your skills and allow us to learn more about your personality.</p>
     `
		},
		question3: {
			q: 'What are the rules for writing a good CV?',
			a: `
      <p>Here are the rules to follow to write a good CV:</p>

			<h3>Organize your CV</h3>
			<p>The different sections of a CV help to organize your CV and make it easier to read. It is therefore important to create different and visible headings. and you should not include all the information in one block.</p>

			<h3>Explain your professional experience</h3>
			<p>When writing your CV, always try to get to the point by explaining your professional experience. Give concrete and quantified examples and use simple words and avoid acronyms unless they are really essential.</p>

			<h3>Avoid overloading your CV</h3>
			<p>Sort out the information you want to mention in order to show only that which is of real interest for the position you are considering. We advise you not to exceed two A4 pages. Beyond that, recruiters may not be motivated to read your entire CV.</p>

      <h3>Write for ATS (Applicant Tracking Systems)</h3>
			<p>ATSs are software programs that screen resumes. To do this, they evaluate the compatibility of your resume with the job ad. As a result, your resume should contain only the skills that are most relevant to the position. Therefore, analyze the job ad and try to write a customized CV that perfectly meets the company's needs.</p>

			<h3>Eliminate all mistakes</h3>
			<p>The spelling must be impeccable in order to give the best impression to the recruiters. Therefore, you should read your CV several times to check if there are any mistakes. You should even ask for the opinion of someone close to you. </p>

			<h3>Take care of the layout</h3>
			<p>The form counts as much as the content! It is therefore important to take care of the layout of your CV by creating headings, adjusting the text, and using a font that is pleasant to read in order to make recruiters want to read it.</p>

			<h3>Update your CV regularly</h3>
			<p>Make sure you always update your CV before sending it to recruiters, so that it reflects your current situation.</p>

			<h3>Avoid gaps in your CV</h3>
			<p>Avoid at all costs leaving periods of inactivity where you have not taken any training or gained any work experience, as this will give recruiters a bad impression of you. If you have taken time off to take care of your family or if you have traveled, say so.</p>

			<h3>Put a professional photo</h3>
			<p>If you choose to put a photo on your resume, don't use one of your last vacations! Your resume photo should be professional. It is better to smile. It makes you more likable.</p>

			<h3>Don't exaggerate and be honest</h3>
			<p>When writing your CV, banish phrases such as "I am the right candidate for you" or "I like to think outside the box". These are formulas that exasperate recruiters. Another thing is that you should not lie. In the interview, the recruiter can reveal everything.</p>

			<h3>Never indicate your salary expectations</h3>
			<p>The CV is not the place to indicate your target salary. You will have plenty of time to talk about it during the interview!</p>
     `
		}
	}
};
