export default {
	meta: {
		title: 'Pricing',
		description:
			'Learn about the subscription prices applied by CV Template, a CV maker that helps you create a curriculum vitae using a professional CV template.'
	},
	plans: {
		savingAmount: 'Save up to 20%.',
		bestPlan: 'Popular plan!',
		weekly: {
			feature1: 'Unlimited PDF downloads',
			feature2: 'Unlimited CV link sharing',
			feature3: 'Unlimited creation and modification of CVs',
			feature4: '<span class="all-users-home px-1">7-day</span> subscription',
			feature5: 'No automatic renewal of payment'
		},
		monthly: {
			feature1: 'Unlimited PDF downloads',
			feature2: 'Unlimited sharing of CVs links',
			feature3: 'Unlimited creation and modification of CVs',
			feature4: '<span class="all-users-home px-1">28-day</span> subscription',
			feature5: 'Automatic renewal of payment'
		},
		free: {
			feature1: 'Creation of a single CV',
			feature2: 'Link sharing of a single CV',
			feature3: 'No downloads'
		}
	},
	title: 'Pricing',
	pricing: {
		question1: {
			q: 'How does the subscription work?',
			a: 'If you purchase a subscription, you can edit your CV, create more than one CV and upload your CVs for the duration of the subscription. Once this period is over, features such as CV downloads are no longer allowed. To use them again, you must renew your subscription.'
		},
		question2: {
			q: 'Do I get full access with this subscription? ',
			a: 'Yes, the subscription gives you full access to all features, including CV downloads.'
		},
		question3: {
			q: 'How do I pay? ',
			a: 'Payment can be made by credit card or Paypal. Our payment platform is highly secure.'
		},
		question4: {
			q: 'Is there an automatic renewal of payment?',
			a: 'No. There is no automatic renewal of payment in the case of 7-day subscription. The payment is made only when you click on the pay button and covers 7 days of use. However, in the case of 28-day subscription, there is an automatic renewal that you can stop at any moment.'
		}
	}
};
