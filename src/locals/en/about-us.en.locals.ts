export default {
	meta: {
		title: 'About Us',
		description:
			'Learn about the culture and the philosophy of CV Template, a CV maker that helps you create a curriculum vitae using a professional CV template.'
	},
	aboutUs: `
  <article class="terms-and-conditions container flex flex-col flex-wrap justify-start mdCustom:justify-around items-start mb-8">
    <h1>About Us</h1>
    <p>At Cv-template.co, we believe that creating a professional CV is a necessary step to increase your chances of getting hired. As a job seeker, your career always starts with a good first impression you will give through a well-written CV.</p>
    <p>For this purpose, we have created an easy-to-use website that allows you to not only write an attractive CV but also to download it in PDF format. This CV meets the standards required by today's recruiters as we offer clear guidance on the proper way to complete all sections.</p>
    <p>Every day we work to further improve the user experience on our site by providing new CV templates and user options.</p>
    <p>Many CVs have been created through our site and have helped our users land jobs with reputable companies.</p>
    <br>
  </article>
	`
};
