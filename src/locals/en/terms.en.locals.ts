export default {
	meta: {
		title: 'Terms of service',
		description:
			'Learn about the terms of service used by CV Template, a CV maker that helps you create a curriculum vitae using a professional CV template. '
	},
	terms: `
<article class="terms-and-conditions privacy-policy container flex flex-col flex-wrap justify-start mdCustom:justify-around items-start mb-8">
    <h1>Terms of service</h1>
    <p>
        <a href="https://cv-template.co" target="_blank">Cv-template.co</a> is made available to you with the following terms and conditions. By using Cv-template.co, you agree to all the terms and conditions listed below.
        <br>
        These terms and conditions may be subject to update.
    </p>
    <h2>Article 1. Use of Cv-template.co</h2>
    <p>
        <ol class='article-number-one'>
          <li>Cv-template.co allows you - in exchange for payment - to create, modify and upload one or more CVs.</li>
          <li>Once you have activated your account, you can log in and use our service directly.</li>
          <li>You must limit access to your account by means of a login and password. Indeed, it is your responsibility to maintain the confidentiality of your account. You have full control over all activities that occur under your account unless you have informed us that a third party is aware of your password.</li>
          <li>To use our service, you give us access to your personal data. See the privacy policy for more details.</li>
        </ol>
    </p>
    <h2>Article 2. Terms of use</h2>
    <p>
        <ol class='article-number-two'>
          <li>Accounts are created on an individual basis. Therefore, they are not transferable.</li>
          <li>It is forbidden to use CV Template for illegal purposes, including saving or sharing defamatory or racist information and sending unsolicited messages.</li>
          <li>In case we notice that you do not comply with the above conditions, or if we receive a complaint about it, we have the right to delete your account.</li>
          <li>CV Template has the right to file a complaint at any time if violations on your part are found. It may also claim compensation from you for damages caused by the violation of the terms of use.</li>
          <li>You are 100% responsible for all activities you do online with CV Template account. These activities are not limited to the creation of your CV.</li>
        </ol>
    </p>
    <h2>Article 3. Availability and maintenance</h2>
    <p>
        <ol class='article-number-three'>
          <li>CV Template tries its best to maintain a permanent service, but cannot guarantee that our service is always available.</li>
          <li>CV Template company maintains the site on a recurring basis. Maintenance may take place at any time and may also cause a restriction of access. The maintenance will be announced on the main page of the site.</li>
          <li>CV Template may update the functionality of the site. Your feedback is welcome, but the company Cv-template.co will decide alone which changes will be executed.</li>
        </ol>
    </p>
    <h2>Article 4. Intellectual property</h2>
    <p>
        <ol class='article-number-four'>
          <li>Cv-template.co and its associated CV templates are the property of the Cv-template.co company and its content providers. Any use without the written permission of Cv-template.co is strictly prohibited, except for cases that are allowed by law.</li>
          <li>The information registered by you through our site, such as the content of your CV or your passport photo, is your property. Cv-template.co has a limited right in the processing of this information, which is explained in our privacy policy.</li>
          <li>You can revoke your right as a user at any time by deleting your information and/or terminating your contract.</li>
          <li>You can modify at any time your information registered or published through Cv-template.co.</li>
          <li>If you send information to the Cv-template.co company, for example, suggestions for changes, you give us an unlimited right to use this information. This does not apply to information that you explicitly mention as confidential.</li>
          <li>Throughout the duration of your subscription, you have the right to generate resumes with Cv-template.co. After the termination of your subscription, you are no longer entitled to generate these documents.</li>
        </ol>
    </p>
    <h2>Article 5. Payment of Cv-template.co service</h2>
    <p>
        <ol class='article-number-five'>
          <li>A payment is required to download a CV created with Cv-template.co</li>
          <li>The purchase of a subscription is mandatory to use our service in an unlimited way. The subscription fee is for 7 days of use and will be paid when you click the "pay" button. The payment is made only once. There is no automatic renewal.</li>
          <li>You can consult the current prices for the subscription to our service on our website.</li>
        </ol>
    </p>
    <h2>Article 6. Cancellation</h2>
    <p>
        <ol class='article-number-six'>
          <li>Since your CV is delivered immediately online, at your request, your legal right of withdrawal will no longer apply.</li>
          <li>By buying a subscription, you do not have the right of withdrawal. If you are not satisfied with the subscription, you can terminate the service or delete the account, but no refund will be due upon termination.</li>
        </ol>
    </p>
    <h2>Article 7. Terms of payment</h2>
    <p>
        <ol class='article-number-seven'>
          <li>Payment is possible by credit card or by PayPal. Your account will be activated after receipt of payment by the company Cv-template.co.</li>
          <li>The subscription fee will be charged each time you click on "Pay" button in the case of 7-day subscription. However, in the case of 28-day subscription, there will be an automatic nenewal of payment.</li>
          <li>In case of late or non-payment, Cv-template.co is entitled to restrict its services, for example, the downloading of your CV.</li>
        </ol>
    </p>
    <h2>Article 8. Duration of the contract and termination</h2>
    <p>
        <ol class='article-number-eight'>
          <li>The subscription will be activated from the moment you create your account.</li>
          <li>If you buy a subscription, you can terminate this subscription through your account.</li>
          <li>In case of cancellation of your subscription, the fees paid in advance will never be refunded to the user.</li>
        </ol>
    </p>
    <h2>Article 9. Liability</h2>
    <p>
        <ol class='article-number-nine'>
          <li>The liability provisions listed here will be applicable to the use of Cv-template.co insofar as no legal provision opposes it.</li>
          <li>The liability of Cv-template.co is limited to the amount paid up to three months prior to the event of damage, subject to intent or gross negligence.</li>
          <li>Cv-template.co is not liable for damages caused by management problems at the company level.</li>
          <li>In case of force majeure, the Cv-template.co company is not obliged to compensate you for the damage caused. The force majeure can be an internet or electricity failure, war or national conflict, fire, flood or simply the dysfunction of the company due to management problems.</li>
        </ol>
    </p>
    <h2>Article 10. Changes to the conditions of use</h2>
    <p>
        <ol class='article-number-ten'>
          <li>CV Template can make changes to the conditions listed here, as well as the prices applied, and this to the extent permitted by law.</li>
          <li>CV Template will announce all changes at least 30 days before they take effect on our website, so that you can be informed.</li>
          <li>If there are changes that you do not agree with, you may delete your account and terminate your contract. Use of Cv-template.co after the effective date is considered acceptance of the new modified terms.</li>
        </ol>
    </p>
    <br>
    <br>
    <br>
    <br>
    <h2>Contact</h2>
    <p>
        <ul>
            <li>Address: CV Template, Entrepria Coworking Space, Monastir 5000, Tunisia</li>
            <li>Email: <a href='mailto:hi@cv-template.co'>hi@cv-template.co</a></li>
        </ul>
    </p>
</article>
	`
};
