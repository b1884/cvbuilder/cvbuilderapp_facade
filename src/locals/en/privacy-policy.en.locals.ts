export default {
	meta: {
		title: 'Privacy Policy',
		description:
			'Learn about CV Template privacy policy, which complies with the General Data Protection Regulation guidelines.'
	},
	policy: `
<article class="terms-and-conditions container flex flex-col flex-wrap justify-start mdCustom:justify-around items-start mb-8">
    <h1>Privacy Policy</h1>
    <p>
        <a href="https://cv-template.co" target="_blank">Cv-template.co</a> complies with the General Data Protection Regulation (GDPR) as enacted by the European Parliament on April 27, 2016, and implemented from May 25, 2018.
    </p>
    <p>
    This means that:
        <br>
        <ul>
            <li>We do not collect sensitive or critical data of a very private nature;</li>
            <li>We limit our data collection to personal data provided by the user and which are necessary for legitimate purposes;</li>
            <li>We do not use this data for any purpose other than to create your CV;</li>
            <li>We ask for the user permission (consent) to place cookies;</li>
            <li>We use all necessary security measures in order to protect your personal data;</li>
            <li>We respect your right to have your personal data reviewed, corrected, or deleted.</li>
        </ul>
    </p>
    <p>
    In this privacy policy, we explain what personal data we process, and for what purpose. We also explain our cookie policy. Therefore, we encourage you to read all the details explained in this page.
    </p>
    <h2>Processing of user’s personal data</h2>
    <p>
    By using Cv-template.co, you will provide us with some of your personal data. We only store and process personal data that you actively or passively provide in the context of a CV creation service that you have requested or used. Therefore, you have given us the right to process your data in this way.
    </p>
    <h2>Creation & Registration of CVs</h2>
    <p>
    By creating a CV, you automatically create an account in Cv-template.co. This account is included in your subscription. We use the following data to create your CV:
        <br>
        <ul>
            <li>Name and address</li>
            <li>Phone number</li>
            <li>Billing address</li>
            <li>Email address</li>
            <li>Payment information</li>
            <li>Gender/li>
            <li>Your photo</li>
            <li>Date of birth</li>
            <li>IP address</li>
            <li>Information as mentioned on your CV, such as work experience and education</li>
        </ul>
    </p>
    <p>
    When you register, we save the personal data you have provided in our database. We already save them from the moment you start creating an account.
    </p>
    <p>
    You can log in to your account with the e-mail address and password that you have given. We save this data so that you do not have to enter it each time, and so that we can contact you in connection with the execution of the agreement, billing, and payment.
    </p>
    <p>CV Template processes your personal data to save your progress, in particular the CVs you have created, in order to make them available to you when using our services in the future. </p>
    <p>If you have not completed the resume, we may send you reminder emails.</p>
    <h2>Data retention duration</h2>
    <p><CV Template saves your personal data for as long as it is needed to fulfill your modification or deletion purposes.  </p>
    <p>Thus, we keep your personal data until you request to delete your account. </p>
    <p>If you keep your account inactive for a long period of time, CV Template may contact you to ask whether your personal data should be saved.</p>
    <h2>Provision of data to third parties</h2>
    <p>Be sure that we do not give your data to any third party.</p>
    <p>However, social network accounts are linked to our website. The administrators of these services may collect some of your personal data. So, you need to verify their privacy policies.</p>
    <h2>Cookies</h2>
    <p>Cookies are small information files that are saved when visiting a website or that can be read from the visitor's device.</p>
    <p>When visiting CV Template, a pop-up will appear asking you to accept the installation of cookies.</p>
    <p>From the moment you click "I accept", you give us permission to use Google Analytics cookies. However, you are always free to delete the cookies by using an appropriate plugin.</p>
    <h3>Google Analytics</h3>
    <p>We use cookies to gain an overview of the use of our website and to improve this use with the help of this overview.</p>
    <p>For this purpose, we use Google Analytics to record how visitors behave on our website.</p>
    <p>In our agreement with Google, we have not allowed Google to use these informations for other services and finally, the IP addresses will be made anonymous. You can find more details about data processing if you consult <a href="http://www.google.com/intl/fr/policies/privacy/" target="_blank" rel="nofollow">the privacy policy of Google</a>. </p>
    <p>According to Google, this data is stored for 2 years.</p>
    <h2>Data security</h2>
    <p>We are taking all appropriate and adequate security measures to limit abuse and unauthorized access to your personal data</p>

    <h2>Third-Party Sites</h2>
    <p>The privacy and cookie policy you are reading, does not apply to third-party websites that are connected to our website through links.</p>
    <p>For this reason, we cannot guarantee that third parties process your personal data in a reliable and a secure manner.</p>
    <p>So, we advise you to investigate the privacy policy of these sites before using their services.</p>

    <h2>Changes to the Privacy Policy</h2>
    <p>We have the right to make changes to this privacy and cookie policy. You should check this privacy and cookie policy regularly to stay informed of any changes.</p>
    <h2>Your rights</h2>
    <p>
        <ul>
            <li>Obtain an explanation of the personal data we hold;</li>
            <li>Request to correct errors;</li>
            <li>Request to delete completely personal data;</li>
            <li>Withdraw authorization or permanently delete your account;</li>
        </ul>
    </p>
    <p>To inquire further about our policy or to send a complaint, you can contact us at any time via the contact information below and we will do our best to properly address your needs</p>
    <br>
    <br>
    <br>
    <br>
    <h2>Contact</h2>
    <p>
        <ul>
            <li>Address: CV Template, Entrepria Coworking Space, Monastir 5000, Tunisia</li>
            <li>Email: <a href='mailto:hi@cv-template.co'>hi@cv-template.co</a></li>
        </ul>
    </p>
</article>`
};
