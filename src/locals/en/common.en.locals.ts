export default {
	misc: {
		login: 'Login',
		signup: 'Sign up',
		createCv: 'Create a CV',
		createBestCV: 'Create your best CV',
		selectTemplate: 'Select a CV template',
		blog: 'Blog',
		faq: 'FAQ',
		myAccount: 'My account',
		languages: 'Languages: '
	},
	buttons: {
		french: 'French',
		english: 'English',
		arabic: 'Arabic',
		continue: 'Continue',
		back: 'Back'
	}
};
