export default {
	meta: {
		title: 'Best CV templates by CV Template',
		description:
			'Use the best CV templates offered by our CV maker that helps you build a curriculum vitae which seduces recruiters.'
	},
	title: 'Best CV templates',
	description:
		'Each CV template is designed by experts and follows the best practices that hiring managers look for. Stand out from the crowd and get hired faster with simple, modern and attractive CV templates.',
	choseThisModel: '{{usersNumber}}+ users have chosen this template',
	useThisModel: 'Use this template'
};
