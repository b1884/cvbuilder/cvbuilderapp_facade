import common from './common.en.locals';
import landing from './landing.en.locals';
import auth from './auth.en.locals';
import terms from './terms.en.locals';
import privacy from './privacy-policy.en.locals';
import pricing from './pricing.en.locals';
import about from './about-us.en.locals';
import faq from './faq.en.locals';
import templates from './templates.en.locals';

export default {
	landing,
	common,
	auth,
	terms,
	pricing,
	privacy,
	about,
	faq,
	templates
};
