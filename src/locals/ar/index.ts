import common from './common.ar.locals';
import landing from './landing.ar.locals';
import auth from './auth.ar.locals';
import terms from './terms.ar.locals';
import privacy from './privacy-policy.ar.locals';
import pricing from './pricing.ar.locals';
import about from './about-us.ar.locals';
import faq from './faq.ar.locals';
import templates from './templates.ar.locals';

export default {
	landing,
	common,
	auth,
	terms,
	pricing,
	privacy,
	about,
	faq,
	templates
};
