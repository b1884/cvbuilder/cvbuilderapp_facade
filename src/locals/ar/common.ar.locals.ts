export default {
	misc: {
		login: 'تسجيل الدخول',
		signup: 'اشتراك',
		createCv: 'أنشئ سيرة ذاتية',
		createBestCV: 'أنشئ سيرة ذاتية',
		selectTemplate: 'إختر نموذج',
		blog: 'مدونة',
		faq: 'التعليمات',
		myAccount: 'حسابي',
		languages: 'اللغات: '
	},
	buttons: {
		french: 'الفرنسية',
		english: 'الإنجليزية',
		arabic: 'العربية',
		continue: 'واصل',
		back: 'إلى الوراء'
	}
};
