export default {
	meta: {
		title: 'نموذج سيرة ذاتية جاهز للتعبئة pdf',
		description:
			'إستعمل نموذج سيرة ذاتية جاهزة للتعبئة pdf ثم قم بتحميله. أظهر المهارات الشخصية عن طريق نماذج سيرة ذاتية جاهزة و احترافية  تضمن لك الحصول على مقابلة شخصية.',
		keywords: 'نموذج سيرة ذاتية جاهزة للتعبئة pdf, سيرة ذاتية , نماذج سيرة ذاتية جاهزة '
	},
	hero: {
		title: 'أنشئ سيرة ذاتية تضمن لك مقابلة شخصية',
		description: 'إختر نموذجا و أنشئ سيرة ذاتية إحترافية تحترم كل المواصفات المطلوبة اليوم في سوق الشغل.',
		liveCounter: '{{cvs}} سيرة صممت اليوم.'
	},
	inDepthDescription: {
		title: 'أنشئ سيرة ذاتية إحترافية وعصرية واحصل على مقابلة شخصية',
		description:
			'أن تجد عملاً يبدو صعباً اليوم. نحن هنا لتسهيل الامر.<br>' +
			'بعد دراسة مئات السير الذاتية كونا تصميما متلائماً مع كل المواصفات المطلوبة اليوم في سوق الشغل. <br><br>' +
			'إستعمل النموذج الموفر واحترم نصائح الكتابة لتتمكن من إنشاء سيرة ذاتية احترافية وفعالة تبرز كل مهاراتك الشخصية وخبراتك المهنية وتساعدك على ضمان مقابلة شخصية.'
	},
	icon1: {
		title: 'أكثر من مجرد تصميم سيرة ذاتية',
		description:
			'السيرة الذاتية القوية تصنع الفارق عندما يكون لديك الكثير من المنافسين على نفس الوظيفة. لذلك،إضافةً إلى التصميم، سنوجهك عن طريق نصائح واضحة حتى تتمكن من كتابة سيرة ذاتية قادرة على جلب إنتباه المشغلين.'
	},
	icon2: {
		title: 'تصميم سيرة ذاتية أمر سهل للغاية',
		description:
			'لن تحتاج إلى ساعات طويلة لإنشاء وتصميم سيرتك الذاتية. فقط انقر على التصميم الذي يعجبك و املأ المعلومات لتكون سيرتك الذاتية جاهزة خلال دقائق.'
	},
	icon3: {
		title: 'قوي حظوظك في الحصول على وظيفة',
		description:
			'استعنّا بخبرات المُختصّين في مجال الموارد البشرية كي نوفر لك نماذج وقوالب سيرة ذاتية عملية تساعدك على عرض مهاراتك الشخصية و خبراتك  المهنية بأفضل طريقة ممكنة.'
	},
	templates: {
		title: 'أفضل نموذج سيرة ذاتية جاهزة للتعبئة',
		description:
			'النماذج المستعملة ستمكنك من تصميم سيرة إحترافية و عصرية جاهزة للتحميلPDF ' +
			'<br> <br>' +
			'هذا يضمن حفظ بياناتك و عرض سيرتك الذاتية كما صممّتها بغض النظر عن نظام التشغيل المًستخدم'
	},
	reviews: {
		title: 'ستكون 100% راضٍ على سيرتك!',
		rev1: {
			name: 'آدم',
			job: 'مطور برمجيات',
			review: 'علمني برو سيفي دزينر كيف اكتب سيرة ذاتية متوافقة مع نظم ATS و هي البرامج التي تستعملها الشركات اليوم لتسهيل فرز و اختيار أفضل المترشحين لعرض الشغل. لذا انصح بأستعمال برو سيفي دزينر بكل ثقة.'
		},
		rev2: {
			name: 'مريم',
			job: 'ممرضة',
			review: 'الفرق بين برو سيفي دزينر و غيره من الحلول أنه لا يوفر فقط تصميماً للسيرة الذاتية بل يعطي أيضاً نصائحاً واضحة في الكتابة. وفي رأيي هذا ما يبحث عنه كل طالبي الشغل. شكراً جزيلاً!'
		},
		rev3: {
			name: 'إلياس',
			job: 'مسؤول مبيعات',
			review: 'بفضل تصميم  برو سيفي دزينر تمكنت من الحصول على وظيفة مسؤول مبيعات بشركة ناشئة. لذا نصحت جميع اصدقائي الباحثين عن عمل بأستعمال برو سيفي دزينر. لا تفوتوا هذه الفرصة.'
		}
	},
	lastCTA: {
		title: 'انضموا إلى أكثر من <span class="all-users-home">&nbsp;12,000&nbsp;</span> مستخدم راض!',
		description:
			'لا تتردد و أبدأ أول خطوة من أجل الحصول على الوظيفة التي تحلم بها. أنشئ أفضل سيرة ذاتية لديك مع  برو سيفي دزينر.'
	},
	footer: {
		copyrights: 'فلننهي البطالة معاً © <a href="{{website}}">Cv-template.co</a> {{year}} 🤘',
		section1: {
			title: 'كن على إتصالٍ معنا عن طريق'
		},
		section3: {
			title: 'مساعدة',
			faq: 'الأسئلة الأكثر تداولا',
			contactUs: 'اتصل بنا',
			privacy: 'سياسة الخصوصية',
			terms: 'مجال الخدمات',
			about: 'من نحن'
		},
		section2: {
			title: 'برنامجنا',
			pricing: 'الأسعار',
			models: 'النماذج',
			createCv: 'تصميم سيرة ذاتية',
			blog: 'مدونه'
		}
	},
	cookieBanner: {
		message: 'هل يمكننا إستخدام الكوكيز ؟ تفقد ',
		privacyLink: 'سياسة الخصوصية الخاصة بنا',
		accept: 'أقبل'
	}
};
