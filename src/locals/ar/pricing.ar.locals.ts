export default {
	meta: {
		title: 'الرسوم الواجب دفعها',
		description:
			'إكتشف الرسوم الواجب دفعها لإستخدام برو سيفي دزينر موقع ويب يمكنك من إنشاء سيرة ذاتية إحترافية و جذابة تزيد من فرصك في الحصول على وظيفة.'
	},
	plans: {
		savingAmount: 'توفير حتى 20%.',
		bestPlan: 'إشترك شائع!',
		weekly: {
			feature1: 'تحميل ملفات بي دي إف غير محدود',
			feature2: 'مشاركات روابط سير ذاتية غير محدود',
			feature3: 'إنشاء وتعديل غير محدودين للسير الذاتية',
			feature4: 'إشتراك لمدة <span class="all-users-home px-1">7 أيام</span>',
			feature5: 'ليس هناك تجديد تلقائي للدفع'
		},
		monthly: {
			feature1: 'تحميل ملفات بي دي إف غير محدود',
			feature2: 'مشاركات روابط سير ذاتية غير محدود',
			feature3: 'إنشاء وتعديل غير محدودين للسير الذاتية',
			feature4: 'إشتراك لمدة <span class="all-users-home px-1">28 أيام</span>',
			feature5: 'هناك تجديد تلقائي للدفع'
		},
		free: {
			feature1: 'إنشاء سيرة ذاتية واحدة',
			feature2: 'مشاركة رابط سيرة ذاتية واحدة',
			feature3: 'لا تحميل'
		}
	},
	title: 'الرسوم الواجب دفعها',
	pricing: {
		question1: {
			q: 'كيف يعمل الاشتراك؟',
			a: 'إذا قمت بشراء اشتراك ، يمكنك تعديل سيرتك الذاتية وإنشاء أكثر من سيرة ذاتية وتحميل سيرتك الذاتية لمدة 7 أيام أو 28 يوم. بمجرد انتهاء هذه الفترة ، لم يعد مسموحًا بالتمتع ببعض الخدمات مثل تحميل السيرة الذاتية. لاستخدامها مرة أخرى ، يجب عليك تجديد اشتراكك.'
		},
		question2: {
			q: 'هل يمكنني هذا الاشتراك من التمتع بجميع الخدمات؟ ',
			a: 'نعم ، يمنحك الاشتراك التمتع بجميع الخدمات ، بما في ذلك تحميل السيرة الذاتية.'
		},
		question3: {
			q: 'كيف أقوم بدفع الرسوم؟ ',
			a: 'يمكن السداد عن طريق بطاقة الائتمان أو Paypal. منصة الدفع الخاصة بنا آمنة.'
		},
		question4: {
			q: 'هل هناك تجديد تلقائي للدفع؟',
			a: "لا ، لا يوجد تجديد تلقائي للدفع بالنسبة لإشتراك 7 أيام.. يتم السداد فقط عند النقر فوق الزر 'دفع' ويغطي 7 أيام من الاستخدام.أما بالنسبة للإشتراك الشهري فهناك تجديد تلقائي للدفع يمكنك توقيفه متى شئت."
		}
	}
};
