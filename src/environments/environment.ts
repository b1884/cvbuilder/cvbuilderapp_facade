// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
	production: false,
	appName: 'CV Designer',
	appNameEn: 'CV Template',
	builderApp: 'https://www.app.cv-template.xyz',
	builderApi: 'https://www.api.cv-template.xyz',
	blog: 'https://cv-template.co/blog',
	mainUrl: 'https://www.cv-template.xyz',
	linkedinAuthUrl: 'https://www.linkedin.com/oauth/v2/authorization',
	linkedinClientId: '78htglebyfggod',
	googleClientId: '412972654203-9qin2sit9t9m6104cesst0pbjuc2o17b.apps.googleusercontent.com'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
