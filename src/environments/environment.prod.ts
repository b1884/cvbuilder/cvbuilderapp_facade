export const environment = {
	production: true,
	appName: 'CV Designer',
	appNameEn: 'CV Template',
	builderApp: 'https://app.cv-template.co',
	builderApi: 'https://suwoxoxoti22.cv-template.co/api',
	blog: 'https://cv-template.co/blog',
	mainUrl: 'https://cv-template.co',
	linkedinAuthUrl: 'https://www.linkedin.com/oauth/v2/authorization',
	linkedinClientId: '78htglebyfggod',
	googleClientId: '412972654203-9qin2sit9t9m6104cesst0pbjuc2o17b.apps.googleusercontent.com'
};
