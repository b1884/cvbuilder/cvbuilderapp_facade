import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LandingWrapperComponent } from './landing-wrapper/landing-wrapper.component';
import { HomeComponent } from './pages/home/home.component';
import { TermsAndConditionsPageComponent } from './pages/terms-and-conditions-page/terms-and-conditions-page.component';
import { FaqPageComponent } from './pages/faq-page/faq-page.component';
import { PrivacyPolicyPageComponent } from './pages/privacy-policy-page/privacy-policy-page.component';
import { AboutUsPageComponent } from './pages/about-us-page/about-us-page.component';
import { TemplatesPageComponent } from './pages/templates-page/templates-page.component';
import { PricingPageComponent } from './pages/pricing-page/pricing-page.component';
import { LanguageGuard } from '../../core/guards/language.guard';

const routes: Routes = [
	{
		path: '',
		component: LandingWrapperComponent,
		children: [
			{
				path: '',
				canActivate: [LanguageGuard],
				component: HomeComponent,
				data: { lang: 'ar' }
			},
			{
				path: 'نموذج-سيرة-ذاتية-جاهز-للتعبئة-pdf',
				component: HomeComponent,
				data: { lang: 'ar' }
			},
			{
				path: 'formatting-a-cv',
				component: HomeComponent,
				data: { lang: 'en' }
			},
			{
				path: 'cv-designer',
				component: HomeComponent,
				data: { lang: 'fr' }
			},
			{
				path: 'شروط-الاستخدام',
				component: TermsAndConditionsPageComponent,
				data: { lang: 'ar' }
			},
			{
				path: 'conditions-generales',
				component: TermsAndConditionsPageComponent,
				data: { lang: 'fr' }
			},
			{
				path: 'terms-of-service',
				component: TermsAndConditionsPageComponent,
				data: { lang: 'en' }
			},
			{
				path: 'تصميم-سيرة-ذاتية',
				component: FaqPageComponent,
				data: { lang: 'ar' }
			},
			{
				path: 'faire-un-cv',
				component: FaqPageComponent,
				data: { lang: 'fr' }
			},
			{
				path: 'how-to-make-cv',
				component: FaqPageComponent,
				data: { lang: 'en' }
			},
			{
				path: 'سياسة-الخصوصية',
				component: PrivacyPolicyPageComponent,
				data: { lang: 'ar' }
			},
			{
				path: 'politique-confidentialite',
				component: PrivacyPolicyPageComponent,
				data: { lang: 'fr' }
			},
			{
				path: 'privacy-policy',
				component: PrivacyPolicyPageComponent,
				data: { lang: 'en' }
			},
			{
				path: 'من-نحن',
				component: AboutUsPageComponent,
				data: { lang: 'ar' }
			},
			{
				path: 'a-propos',
				component: AboutUsPageComponent,
				data: { lang: 'fr' }
			},
			{
				path: 'about-us',
				component: AboutUsPageComponent,
				data: { lang: 'en' }
			},
			{
				path: 'نماذج-سيرة-ذاتية',
				component: TemplatesPageComponent,
				data: { lang: 'ar' }
			},
			{
				path: 'modele-cv',
				component: TemplatesPageComponent,
				data: { lang: 'fr' }
			},
			{
				path: 'cv-examples',
				component: TemplatesPageComponent,
				data: { lang: 'en' }
			},
			{
				path: 'التعريفة',
				component: PricingPageComponent,
				data: { lang: 'ar' }
			},
			{
				path: 'tarifs',
				component: PricingPageComponent,
				data: { lang: 'fr' }
			},
			{
				path: 'pricing',
				component: PricingPageComponent,
				data: { lang: 'en' }
			}
		]
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class LandingRouting {}
