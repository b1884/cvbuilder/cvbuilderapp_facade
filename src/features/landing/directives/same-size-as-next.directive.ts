import { AfterViewChecked, Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
	selector: '[blackbirdSameSizeAsNext]'
})
export class SameSizeAsNextDirective implements AfterViewChecked {
	constructor(public elementRef: ElementRef<HTMLElement>) {}

	ngAfterViewChecked(): void {
		this._resizeElement();
	}

	@HostListener('window:resize')
	private _resizeElement(): void {
		const neighbor = this.elementRef.nativeElement.nextElementSibling as HTMLElement;

		if (!neighbor || !neighbor.style) {
			return;
		}
		this.elementRef.nativeElement.style.height = `${neighbor.offsetHeight}px`;
		this.elementRef.nativeElement.style.width = `${neighbor.offsetWidth}px`;
	}
}
