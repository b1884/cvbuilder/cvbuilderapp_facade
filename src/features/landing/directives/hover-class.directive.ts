import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
	selector: '[blackbirdHoverClass]'
})
export class HoverClassDirective {
	constructor(public elementRef: ElementRef<HTMLElement>) {}
	@Input('blackbirdHoverClass') hoverClass!: string;

	@HostListener('mouseenter') onMouseEnter(): void {
		if (!this.hoverClass) {
			return;
		}

		this.elementRef.nativeElement.classList.add(this.hoverClass);
	}

	@HostListener('mouseleave') onMouseLeave(): void {
		if (!this.hoverClass) {
			return;
		}

		this.elementRef.nativeElement.classList.remove(this.hoverClass);
	}
}
