import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LandingWrapperComponent } from './landing-wrapper/landing-wrapper.component';
import { HomeComponent } from './pages/home/home.component';
import { SharedModule } from '../../shared/shared.module';
import { LandingRouting } from './landing.routing';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';
import { SideNavComponent } from './components/side-nav/side-nav.component';
import { TermsAndConditionsPageComponent } from './pages/terms-and-conditions-page/terms-and-conditions-page.component';
import { FaqPageComponent } from './pages/faq-page/faq-page.component';
import { FooterComponent } from './components/footer/footer.component';
import { PrivacyPolicyPageComponent } from './pages/privacy-policy-page/privacy-policy-page.component';
import { AboutUsPageComponent } from './pages/about-us-page/about-us-page.component';
import { TemplatesPageComponent } from './pages/templates-page/templates-page.component';
import { HoverClassDirective } from './directives/hover-class.directive';
import { SameSizeAsNextDirective } from './directives/same-size-as-next.directive';
import { PricingPageComponent } from './pages/pricing-page/pricing-page.component';
import { SameSizeAsPreviousDirective } from './directives/same-size-as-previous.directive';

@NgModule({
	declarations: [
		LandingWrapperComponent,
		HomeComponent,
		NavBarComponent,
		SideNavComponent,
		TermsAndConditionsPageComponent,
		FaqPageComponent,
		FooterComponent,
		PrivacyPolicyPageComponent,
		AboutUsPageComponent,
		TemplatesPageComponent,
		HoverClassDirective,
		SameSizeAsNextDirective,
		PricingPageComponent,
		SameSizeAsPreviousDirective
	],
	imports: [CommonModule, SharedModule, LandingRouting]
})
export class LandingModule {}
