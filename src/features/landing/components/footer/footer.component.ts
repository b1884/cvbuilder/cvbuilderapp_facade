import { Component } from '@angular/core';
import { BaseComponent } from '../../../../shared/classes/base-component.class';
import { environment } from '../../../../environments/environment';

@Component({
	selector: 'blackbird-footer',
	templateUrl: './footer.component.html',
	styleUrls: ['./footer.component.scss']
})
export class FooterComponent extends BaseComponent {
	year = new Date().getFullYear();
	blogUrl = environment.blog;
	mainUrl = environment.mainUrl;

	constructor() {
		super();
	}
}
