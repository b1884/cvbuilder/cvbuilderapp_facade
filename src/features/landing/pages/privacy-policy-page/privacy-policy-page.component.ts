import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../../../shared/classes/base-component.class';
import { firstValueFrom } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { LangType } from '../../../../types/lang.types';

@Component({
	selector: 'blackbird-privacy-policy-page',
	templateUrl: './privacy-policy-page.component.html',
	styleUrls: ['./privacy-policy-page.component.scss']
})
export class PrivacyPolicyPageComponent extends BaseComponent implements OnInit {
	constructor(private _route: ActivatedRoute) {
		super();
	}

	ngOnInit(): void {
		void this._initLangAndMeta();
	}

	private async _initLangAndMeta() {
		await this.setLanguage(this._route);
		await this._initMetaTexts();
	}

	private async _initMetaTexts(): Promise<void> {
		const t = await firstValueFrom(this.translate.get(this.t.privacy.meta.title));
		const title = await firstValueFrom(this.translate.get(this.t.privacy.meta.title));
		const description = await firstValueFrom(this.translate.get(this.t.privacy.meta.description));

		this.metaService.initPageMeta(title, description);
	}
}
