import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../../../shared/classes/base-component.class';
import { lastValueFrom } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
@Component({
	selector: 'blackbird-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.scss']
})
export class HomeComponent extends BaseComponent implements OnInit {
	cvsCreatedToday = Math.floor(Math.random() * (2652 - 2499)) + 2499;

	constructor(private _route: ActivatedRoute) {
		super();
	}

	ngOnInit(): void {
		void this._initLangAndMeta();
	}

	private async _initLangAndMeta() {
		await this.setLanguage(this._route);
		await this._initMetaTexts();
	}

	private async _initMetaTexts(): Promise<void> {
		const t = await lastValueFrom(this.translate.get(this.t.landing.meta.title));
		const title = await lastValueFrom(this.translate.get(this.t.landing.meta.title));
		const description = await lastValueFrom(this.translate.get(this.t.landing.meta.description));
		const keywords = await lastValueFrom(this.translate.get(this.t.landing.meta.keywords));

		this.metaService.initPageMeta(title, description, keywords);
	}

	getIconTexts(index: number): { title: string; description: string } {
		switch (index) {
			case 1:
				return this.t.landing.icon1;
			case 2:
				return this.t.landing.icon2;
			case 3:
				return this.t.landing.icon3;
			default:
				return this.t.landing.icon1;
		}
	}

	getReviewsTexts(index: number): { name: string; review: string; job: string } {
		switch (index) {
			case 1:
				return this.t.landing.reviews.rev1;
			case 2:
				return this.t.landing.reviews.rev2;
			case 3:
				return this.t.landing.reviews.rev3;
			default:
				return this.t.landing.reviews.rev1;
		}
	}
}
