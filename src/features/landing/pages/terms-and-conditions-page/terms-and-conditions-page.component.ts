import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../../../shared/classes/base-component.class';
import { firstValueFrom } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { LangType } from '../../../../types/lang.types';

@Component({
	selector: 'blackbird-terms-and-conditions-page',
	templateUrl: './terms-and-conditions-page.component.html',
	styleUrls: ['./terms-and-conditions-page.component.scss']
})
export class TermsAndConditionsPageComponent extends BaseComponent implements OnInit {
	constructor(private _route: ActivatedRoute) {
		super();
	}

	ngOnInit(): void {
		void this._initLangAndMeta();
	}

	private async _initLangAndMeta() {
		await this.setLanguage(this._route);
		await this._initMetaTexts();
	}

	private async _initMetaTexts(): Promise<void> {
		const t = await firstValueFrom(this.translate.get(this.t.terms.meta.title));
		const title = await firstValueFrom(this.translate.get(this.t.terms.meta.title));
		const description = await firstValueFrom(this.translate.get(this.t.terms.meta.description));

		this.metaService.initPageMeta(title, description);
	}
}
