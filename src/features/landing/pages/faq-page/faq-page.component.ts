import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../../../shared/classes/base-component.class';
import { firstValueFrom } from 'rxjs';
import { ActivatedRoute } from '@angular/router';

@Component({
	selector: 'blackbird-faq-page',
	templateUrl: './faq-page.component.html',
	styleUrls: ['./faq-page.component.scss']
})
export class FaqPageComponent extends BaseComponent implements OnInit {
	questions = Object.entries(this.t.faq.faq);

	constructor(private _route: ActivatedRoute) {
		super();
	}

	ngOnInit(): void {
		void this._initLangAndMeta();
	}

	private async _initLangAndMeta() {
		await this.setLanguage(this._route);
		await this._initMetaTexts();
	}

	private async _initMetaTexts(): Promise<void> {
		const t = await firstValueFrom(this.translate.get(this.t.faq.meta.title));
		const title = await firstValueFrom(this.translate.get(this.t.faq.meta.title));
		const description = await firstValueFrom(this.translate.get(this.t.faq.meta.description));

		this.metaService.initPageMeta(title, description);
	}
}
