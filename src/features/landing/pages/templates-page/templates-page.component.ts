import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../../../shared/classes/base-component.class';
import { firstValueFrom } from 'rxjs';
import { TemplateInterface } from '../../../../types/template.types';
import { BuilderService } from '../../../../core/services/builder.service';
import { ActivatedRoute } from '@angular/router';

@Component({
	selector: 'blackbird-templates-page',
	templateUrl: './templates-page.component.html',
	styleUrls: ['./templates-page.component.scss']
})
export class TemplatesPageComponent extends BaseComponent implements OnInit {
	templates!: TemplateInterface[] | null;

	constructor(private _builderService: BuilderService, private _route: ActivatedRoute) {
		super();
	}

	ngOnInit(): void {
		void this._initLangAndMeta();
		void this._getTemplates();
	}

	private async _initLangAndMeta() {
		await this.setLanguage(this._route);
		await this._initMetaTexts();
	}

	private async _initMetaTexts(): Promise<void> {
		const t = await firstValueFrom(this.translate.get(this.t.templates.meta.title));
		const title = await firstValueFrom(this.translate.get(this.t.templates.meta.title));
		const description = await firstValueFrom(this.translate.get(this.t.templates.meta.description));

		this.metaService.initPageMeta(title, description);
	}

	private async _getTemplates(): Promise<void> {
		this.templates = await this._builderService.getTemplatesListAPI();
	}
}
