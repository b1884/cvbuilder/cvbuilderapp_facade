import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../../../shared/classes/base-component.class';
import { firstValueFrom } from 'rxjs';
import { ActivatedRoute } from '@angular/router';

@Component({
	selector: 'blackbird-pricing-page',
	templateUrl: './pricing-page.component.html',
	styleUrls: ['./pricing-page.component.scss']
})
export class PricingPageComponent extends BaseComponent implements OnInit {
	questions = Object.entries(this.t.pricing.pricing);
	weekly = Object.entries(this.t.pricing.plans.weekly);
	monthly = Object.entries(this.t.pricing.plans.monthly);
	free = Object.entries(this.t.pricing.plans.free);

	constructor(private _route: ActivatedRoute) {
		super();
	}

	ngOnInit(): void {
		void this._initLangAndMeta();
	}

	private async _initLangAndMeta() {
		await this.setLanguage(this._route);
		await this._initMetaTexts();
	}

	private async _initMetaTexts(): Promise<void> {
		const t = await firstValueFrom(this.translate.get(this.t.pricing.meta.title));
		const title = await firstValueFrom(this.translate.get(this.t.pricing.meta.title));
		const description = await firstValueFrom(this.translate.get(this.t.pricing.meta.description));

		this.metaService.initPageMeta(title, description);
	}
}
