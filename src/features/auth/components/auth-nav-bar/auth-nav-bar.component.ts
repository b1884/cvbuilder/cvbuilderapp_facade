import { Component } from '@angular/core';
import { BaseComponent } from '../../../../shared/classes/base-component.class';

@Component({
	selector: 'blackbird-auth-nav-bar',
	templateUrl: './auth-nav-bar.component.html',
	styleUrls: ['./auth-nav-bar.component.scss']
})
export class AuthNavBarComponent extends BaseComponent {
	constructor() {
		super();
	}
}
