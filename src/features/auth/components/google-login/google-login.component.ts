import { Component, AfterViewInit, ElementRef, Output, EventEmitter } from '@angular/core';
import { environment } from '../../../../environments/environment';

//TODO: search for google type
declare let google: any;
@Component({
	selector: 'blackbird-google-login',
	template: `<div id="login-google-button"></div>`
})
export class GoogleLoginComponent implements AfterViewInit {
	@Output() onGoogleResponse: EventEmitter<any> = new EventEmitter<any>();
	constructor(private _el: ElementRef) {}

	ngAfterViewInit() {
		google.accounts.id.initialize({
			client_id: environment.googleClientId,
			callback: this.handleResponse.bind(this),
			auto_select: false,
			cancel_on_tap_outside: true
		});
		google.accounts.id.renderButton(this._el.nativeElement.querySelector('#login-google-button'), {
			theme: 'outline',
			size: 'large',
			width: '100%'
		});
	}

	handleResponse(value: any) {
		this.onGoogleResponse.emit(value);
	}
}
