import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthWrapperComponent } from './auth-wrapper/auth-wrapper.component';
import { LoginPageComponent } from './pages/login-page/login-page.component';
import { SignupPageComponent } from './pages/signup-page/signup-page.component';

const routes: Routes = [
	{
		path: '',
		component: AuthWrapperComponent,
		children: [
			{
				path: 'mon-meilleur-cv',
				data: { lang: 'fr' },
				component: LoginPageComponent
			},
			{
				path: 'creer-un-cv',
				data: { lang: 'fr' },
				component: SignupPageComponent
			},
			{
				path: 'cvmaker',
				component: LoginPageComponent,
				data: { lang: 'en' }
			},
			{
				path: 'cvbuilder',
				component: SignupPageComponent,
				data: { lang: 'en' }
			},
			{
				path: 'سيرة-ذاتية-جاهزة',
				component: LoginPageComponent,
				data: { lang: 'ar' }
			},
			{
				path: 'تصميم-سيرة-ذاتية',
				component: SignupPageComponent,
				data: { lang: 'ar' }
			},
			{
				path: '',
				redirectTo: 'cvbuilder',
				pathMatch: 'full'
			}
		]
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class AuthRouting {}
