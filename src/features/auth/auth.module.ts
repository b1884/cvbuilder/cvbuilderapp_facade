import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GoogleLoginComponent } from './components/google-login/google-login.component';
import { LoginPageComponent } from './pages/login-page/login-page.component';
import { AuthWrapperComponent } from './auth-wrapper/auth-wrapper.component';
import { AuthRouting } from './auth.routing';
import { SharedModule } from '../../shared/shared.module';
import { SignupPageComponent } from './pages/signup-page/signup-page.component';
import { AuthNavBarComponent } from './components/auth-nav-bar/auth-nav-bar.component';

@NgModule({
	declarations: [
		LoginPageComponent,
		AuthWrapperComponent,
		SignupPageComponent,
		AuthNavBarComponent,
		GoogleLoginComponent
	],
	imports: [CommonModule, AuthRouting, SharedModule]
})
export class AuthModule {}
