import { Component, OnInit } from '@angular/core';
import { NetworkService } from '../../../../core/services/network.service';
import { BaseComponent } from '../../../../shared/classes/base-component.class';
import { UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { SignUpPayloadInterface, SignUpResponseInterface } from '../../../../types/auth.types';
import { HttpErrorResponse } from '@angular/common/http';
import { firstValueFrom } from 'rxjs';
import { ActivatedRoute, Params } from '@angular/router';
import jwt_decode from 'jwt-decode';

@Component({
	selector: 'blackbird-signup-page',
	templateUrl: './signup-page.component.html',
	styleUrls: ['./signup-page.component.scss']
})
export class SignupPageComponent extends BaseComponent implements OnInit {
	isNameFocused = false;
	isLastNameFocused = false;
	isEmailFocused = false;
	registerForm!: UntypedFormGroup;
	sending = false;

	errors = {
		emailExists: false
	};
	socialMedia = false;

	constructor(private _route: ActivatedRoute, private _networkService: NetworkService) {
		super();
	}

	async ngOnInit(): Promise<void> {
		await this._initLangAndMeta();

		this.registerForm = new UntypedFormGroup({
			name: new UntypedFormControl('', [Validators.required]),
			lastName: new UntypedFormControl('', [Validators.required]),
			email: new UntypedFormControl('', [Validators.required, Validators.email])
		});
		const params: Params = await firstValueFrom(this._route.queryParams);

		if (!params.code && !params.signUpPayload) {
			return;
		}

		if (params.code) {
			this.socialMedia = true;
			const inforFormLinedinResult = await this.authService.getInfoFromLinkedin(params.code, true);
			this.registerForm.patchValue(inforFormLinedinResult);
			await this.register();
		}

		if (params.signUpPayload) {
			this.registerForm.patchValue(params.signUpPayload);
			await this.register();
		}
	}

	async handleCredentialResponse(response: any): Promise<void> {
		// TODO: define response type.
		const responsePayload: any = jwt_decode(response.credential);

		this.registerForm.patchValue({
			name: responsePayload.given_name,
			lastName: responsePayload.family_name,
			email: responsePayload.email
		});
		this.socialMedia = true;
		await this.register();
	}

	getLinkedInLink(): void {
		window.location.href = this._networkService.getLinkedInUrl('/auth/cvbuilder');
	}

	private async _initLangAndMeta() {
		await this.setLanguage(this._route);
		await this._initMetaTexts();
	}

	private async _initMetaTexts(): Promise<void> {
		const title = await firstValueFrom(this.translate.get(this.t.auth.signup.meta.title));
		const description = await firstValueFrom(this.translate.get(this.t.auth.signup.meta.description));
		const keywords = await firstValueFrom(this.translate.get(this.t.auth.signup.meta.keywords));

		this.metaService.initPageMeta(title, description, keywords);
	}

	async register(): Promise<void> {
		const payload: SignUpPayloadInterface = this.registerForm.getRawValue();

		this.loaderService.showLoader();
		this.sending = true;
		this.errors.emailExists = false;

		try {
			const params: Params = await firstValueFrom(this._route.queryParams);

			if (params && params.codeName) {
				payload.codeName = params.codeName;
			}

			const signUpRes: SignUpResponseInterface = await this.authService.registerAPI(payload);

			localStorage.setItem('exp', `${signUpRes.exp}`);
			this.loaderService.hideLoader();
			await this.goToBuilder();
		} catch (e) {
			this.sending = false;
			this.loaderService.hideLoader();

			if (e instanceof HttpErrorResponse) {
				if (e.status === 409) {
					this.errors.emailExists = true;
					if (this.socialMedia) {
						this.socialMedia = false;
						await this.router.navigate(['auth/cvmaker'], { queryParams: { email: payload.email } });
					}
					return;
				}

				if (e && e.error && e.error.message) {
					alert(e.error.message);
				}
				return;
			}
		}
	}
}
