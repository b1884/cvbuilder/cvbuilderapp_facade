import { Component, OnInit } from '@angular/core';
import jwt_decode from 'jwt-decode';
import { NetworkService } from '../../../../core/services/network.service';
import { BaseComponent } from '../../../../shared/classes/base-component.class';
import { HttpErrorResponse } from '@angular/common/http';
import { firstValueFrom } from 'rxjs';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
	selector: 'blackbird-login-page',
	templateUrl: './login-page.component.html',
	styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent extends BaseComponent implements OnInit {
	isFocused = false;
	email = '';

	emailSent = false;
	sending = false;

	errors = {
		emailExists: false
	};
	socialMedia = false;

	signUpPayload:
		| {
				email: string;
				name: string;
				lastName: string;
		  }
		| undefined;

	constructor(private _route: ActivatedRoute, private _networkService: NetworkService) {
		super();
	}

	async ngOnInit(): Promise<void> {
		void this._initLangAndMeta();
		const params: Params = await firstValueFrom(this._route.queryParams);
		if (!params.email && !params.code) {
			return;
		}

		if (params.email) {
			this.email = params.email;
			await this.sendEmail();
		}

		if (params.code) {
			const inforFormLinedinResult = await this.authService.getInfoFromLinkedin(params.code, false);
			this.signUpPayload = inforFormLinedinResult;
			this.email = inforFormLinedinResult.email;
			await this.sendEmail();
		}
	}

	getLinkedInLink(): void {
		window.location.href = this._networkService.getLinkedInUrl('/auth/cvmaker');
	}

	private async _initLangAndMeta() {
		await this.setLanguage(this._route);
		await this._initMetaTexts();
	}

	async handleCredentialResponse(response: any): Promise<void> {
		// TODO: define response type.
		const responsePayload: any = jwt_decode(response.credential);
		this.email = responsePayload.email;
		this.signUpPayload = {
			name: responsePayload.given_name,
			lastName: responsePayload.family_name,
			email: responsePayload.email
		};
		await this.sendEmail();
	}

	private async _initMetaTexts(): Promise<void> {
		const t = await firstValueFrom(this.translate.get(this.t.auth.login.meta.title));
		const title = await firstValueFrom(this.translate.get(this.t.auth.login.meta.title));
		const description = await firstValueFrom(this.translate.get(this.t.auth.login.meta.description));
		const keywords = await firstValueFrom(this.translate.get(this.t.auth.login.meta.keywords));

		this.metaService.initPageMeta(title, description, keywords);
	}

	async sendEmail(): Promise<void> {
		if (!this.email) {
			return;
		}

		this.errors.emailExists = false;

		this.loaderService.showLoader();
		this.sending = true;
		try {
			await this.authService.loginAPI(this.email);
			this.emailSent = true;
			this.loaderService.hideLoader();
		} catch (e) {
			this.sending = false;
			this.loaderService.hideLoader();

			if (e instanceof HttpErrorResponse) {
				if (e.status === 409) {
					this.errors.emailExists = true;
					if (this.socialMedia) {
						this.socialMedia = false;
						await this.router.navigate(['auth/cvbuilder'], {
							queryParams: { signUpPayload: this.signUpPayload }
						});
					}
					return;
				}
			}
		}
	}
}
