import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CvPageComponent } from './pages/cv-page/cv-page.component';
import { PublicNavBarComponent } from './components/public-nav-bar/public-nav-bar.component';
import { PublicCVLinkWrapperComponent } from './public-cvlink-wrapper/public-cvlink-wrapper.component';
import { PublicCvlinkRouting } from './public-cvlink.routing';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
	declarations: [CvPageComponent, PublicNavBarComponent, PublicCVLinkWrapperComponent],
	imports: [CommonModule, PublicCvlinkRouting, SharedModule]
})
export class PublicCVLinkModule {}
