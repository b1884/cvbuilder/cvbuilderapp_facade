import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PublicCVLinkWrapperComponent } from './public-cvlink-wrapper/public-cvlink-wrapper.component';
import { CvPageComponent } from './pages/cv-page/cv-page.component';

const routes: Routes = [
	{
		path: '',
		component: PublicCVLinkWrapperComponent,
		children: [
			{
				path: ':sharingLink',
				component: CvPageComponent
			}
		]
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class PublicCvlinkRouting {}
