import { Component } from '@angular/core';

@Component({
	selector: 'blackbird-public-cvlink-wrapper',
	templateUrl: './public-cvlink-wrapper.component.html',
	styleUrls: ['./public-cvlink-wrapper.component.scss']
})
export class PublicCVLinkWrapperComponent {}
