import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../../../shared/classes/base-component.class';
import { ActivatedRoute, Params } from '@angular/router';
import { BuilderService } from '../../../../core/services/builder.service';

@Component({
	selector: 'blackbird-cv-page',
	templateUrl: './cv-page.component.html',
	styleUrls: ['./cv-page.component.scss']
})
export class CvPageComponent extends BaseComponent implements OnInit {
	sharingLink!: string;
	pagesImages: string[] | null = [];

	constructor(private _route: ActivatedRoute, private _builderService: BuilderService) {
		super();
	}

	ngOnInit(): void {
		this._route.params.subscribe((value: Params) => {
			if (!value || !value.sharingLink) {
				void this.router.navigate([this.routes.landing.main]).catch();
				return;
			}

			this.sharingLink = value.sharingLink;
			void this._initPagesImages();
		});
	}

	async _initPagesImages(): Promise<void> {
		this.loaderService.showLoader();

		try {
			this.pagesImages = await this._builderService.getResumePagesAPI(this.sharingLink);

			if (!this.pagesImages || !this.pagesImages.length) {
				void this.router.navigate([this.routes.landing.main]).catch();
			}

			this.loaderService.hideLoader();
		} catch (e) {
			void this.router.navigate([this.routes.landing.main]).catch();
			this.loaderService.hideLoader();
		}
	}
}
