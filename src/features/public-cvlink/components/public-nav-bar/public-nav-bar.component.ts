import { Component } from '@angular/core';
import { BaseComponent } from '../../../../shared/classes/base-component.class';

@Component({
	selector: 'blackbird-public-nav-bar',
	templateUrl: './public-nav-bar.component.html',
	styleUrls: ['./public-nav-bar.component.scss']
})
export class PublicNavBarComponent extends BaseComponent {
	constructor() {
		super();
	}
}
