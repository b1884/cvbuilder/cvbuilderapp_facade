import { PubSubTopicsEnum } from '../core/enums/pub-sub-topics.enum';

export interface PubSubInterface {
	topic: PubSubTopicsEnum;
	data?: any;
}
