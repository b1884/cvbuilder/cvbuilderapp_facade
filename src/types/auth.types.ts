import { UserInterface } from './user.type';

export interface VerifyEmailPayloadInterface {
	key: string;
}

export interface VerifyNewEmailPayloadInterface {
	verifyEmail: string;
}

export interface SignInPayloadInterface {
	email: string;
}

export interface SignUpPayloadInterface extends UserInterface {
	codeName?: string;
}

export interface SignInResponseInterface {
	iat: number;
	exp: number;
}

export interface LinkedinInfoResponseInterface {
	name: string;
	lastName: string;
	email: string;
}

export interface SignUpResponseInterface extends SignInResponseInterface {
	resumeLink: string;
}
