export interface UserInterface {
	_id?: string;
	name: string;
	lastName: string;
	email: string;
	subscriptionType?: string;
	subscriptionStartTime?: Date;
	creationDate?: Date;
}
