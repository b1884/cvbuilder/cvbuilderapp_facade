import { Injectable } from '@angular/core';
import { NetworkService } from './network.service';
import {
	LinkedinInfoResponseInterface,
	SignInPayloadInterface,
	SignInResponseInterface,
	SignUpPayloadInterface,
	SignUpResponseInterface,
	VerifyEmailPayloadInterface
} from '../../types/auth.types';
import { getAuthURL } from '../config/api-routes.config';
import { firstValueFrom } from 'rxjs';
import { ResponseInterface } from '../../types/response.types';
import { environment } from '../../environments/environment';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
	providedIn: 'root'
})
export class AuthService {
	signUpRes!: SignUpResponseInterface;

	constructor(private _http: NetworkService, private _cookieService: CookieService) {}

	async registerAPI(user: SignUpPayloadInterface): Promise<SignUpResponseInterface> {
		const res: ResponseInterface<SignUpResponseInterface> = await firstValueFrom<
			ResponseInterface<SignUpResponseInterface>
		>(this._http.post<SignUpPayloadInterface, SignUpResponseInterface>(getAuthURL('signUp'), user));

		if (!res.success || !res.body) {
			throw new Error(res.message || 'Request failed.');
		}

		this.signUpRes = res.body;
		return res.body;
	}

	async loginAPI(email: string): Promise<void> {
		const res: ResponseInterface = await firstValueFrom<ResponseInterface>(
			this._http.post<SignInPayloadInterface>(getAuthURL('signIn'), { email })
		);

		if (!res.success) {
			throw new Error(res.message || 'Request failed.');
		}

		return;
	}

	async verifyEmailAPI(key: string): Promise<SignInResponseInterface> {
		const res: ResponseInterface<SignInResponseInterface> = await firstValueFrom<
			ResponseInterface<SignInResponseInterface>
		>(this._http.post<VerifyEmailPayloadInterface, SignInResponseInterface>(getAuthURL('verifyEmail'), { key }));

		if (!res.success || !res.body) {
			throw new Error(res.message || 'Request failed.');
		}

		return res.body;
	}

	async logoutAPI(): Promise<void> {
		const res: ResponseInterface = await firstValueFrom<ResponseInterface>(
			this._http.post(getAuthURL('signOut'), {})
		);

		if (!res.success || !res.body) {
			throw new Error(res.message || 'Request failed.');
		}

		return;
	}

	async checkIfEmailExist(email: string): Promise<boolean> {
		const res: ResponseInterface<boolean> = await firstValueFrom<ResponseInterface<boolean>>(
			this._http.post(getAuthURL('checkIfEmailExist'), { email })
		);

		if (!res.success || !res.body) {
			throw new Error(res.message || 'Request failed.');
		}

		return res.body;
	}

	async getInfoFromLinkedin(code: string, signUp: boolean): Promise<LinkedinInfoResponseInterface> {
		const res: ResponseInterface<LinkedinInfoResponseInterface> = await firstValueFrom<
			ResponseInterface<LinkedinInfoResponseInterface>
		>(this._http.post(getAuthURL('getLinkedinToken'), { code, signUp }));

		if (!res.success || !res.body) {
			throw new Error(res.message || 'Request failed.');
		}

		return res.body;
	}

	isTokenValid(): boolean {
		if (!this._cookieService.check('exp')) {
			return false;
		}

		const exp = this._cookieService.get('exp');

		if (!exp) {
			return false;
		}

		return new Date() < new Date(+exp * 1000);
	}

	getBuilderURL(): string {
		if (!this.signUpRes) {
			return environment.builderApp;
		}

		return `${environment.builderApp}/builder/${this.signUpRes.resumeLink}`;
	}
}
