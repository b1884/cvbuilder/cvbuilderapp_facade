import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Params } from '@angular/router';
import { environment } from '../../environments/environment';
import { GenericType } from '../../types/generic.type';
import { ResponseInterface } from '../../types/response.types';
import { Observable } from 'rxjs';

@Injectable({
	providedIn: 'root'
})
export class NetworkService {
	constructor(private _http: HttpClient) {}

	private static _setQueryParams<T = GenericType>(query: null | T): string {
		if (!query) {
			return '';
		}

		const keys = Object.keys(query) as Array<keyof T>;
		if (!keys || !keys.length) {
			return '';
		}

		let queryParams = '?';
		keys.forEach((key: keyof T) => {
			let value: unknown = query[key];

			if (!value) {
				return;
			}

			if (typeof value === 'number') {
				value = `${value}` as unknown;
			}

			if (typeof value === 'string') {
				value = value.trim() as unknown;
			}

			const sParameter = encodeURIComponent(value as string);
			queryParams += `${key as string}=${sParameter}&`;
		});

		queryParams = queryParams.slice(0, -1);
		return queryParams;
	}

	buildUrl(url: string, params: Params): string {
		return `${url}${params ? NetworkService._setQueryParams(params) : ''}`;
	}

	getLinkedInUrl(url: string): string {
		const params = {
			response_type: 'code',
			state: 987654321,
			scope: 'r_liteprofile r_emailaddress',
			client_id: environment.linkedinClientId,
			redirect_uri: `${environment.mainUrl}${url}`
		};

		return this.buildUrl(environment.linkedinAuthUrl, params);
	}

	post<T = GenericType, R = GenericType>(
		url: string,
		body: null | T,
		params?: HttpParams,
		headers?: HttpHeaders
	): Observable<ResponseInterface<R>> {
		return this._http.post<ResponseInterface<R>>(url, body, { headers, params, withCredentials: true });
	}

	get<T = GenericType, R = GenericType>(url: string, params?: null | T): Observable<ResponseInterface<R>> {
		return this._http.get<ResponseInterface<R>>(
			`${url}${params ? NetworkService._setQueryParams<T>(params) : ''}`,
			{ withCredentials: true }
		);
	}

	put<T = GenericType, R = GenericType>(
		url: string,
		body: FormData | T | null,
		params?: HttpParams,
		headers?: HttpHeaders
	): Observable<ResponseInterface<R>> {
		return this._http.put<ResponseInterface<R>>(url, body, { headers, params, withCredentials: true });
	}

	delete<R = GenericType>(url: string, params?: HttpParams, headers?: HttpHeaders): Observable<ResponseInterface<R>> {
		return this._http.delete<ResponseInterface<R>>(url, { headers, params, withCredentials: true });
	}
}
