import { Injectable } from '@angular/core';
import { NetworkService } from './network.service';
import { UserInterface } from '../../types/user.type';
import { ResponseInterface } from '../../types/response.types';
import { firstValueFrom } from 'rxjs';
import { getAuthURL, getUsersURL } from '../config/api-routes.config';
import { VerifyNewEmailPayloadInterface } from '../../types/auth.types';

@Injectable({
	providedIn: 'root'
})
export class UsersService {
	private _currentUser!: UserInterface | null;

	constructor(private _http: NetworkService) {}

	async currentUserAPI(): Promise<UserInterface | null> {
		if (this.currentUser) {
			return this.currentUser;
		}

		const res: ResponseInterface<UserInterface> = await firstValueFrom<ResponseInterface<UserInterface>>(
			this._http.get<unknown, UserInterface>(getUsersURL('current'))
		);

		if (!res.success || !res.body) {
			return null;
		}

		this.currentUser = res.body;
		return res.body;
	}

	async verifyNewEmailAPI(verifyEmail: string): Promise<void> {
		const res: ResponseInterface = await firstValueFrom<ResponseInterface>(
			this._http.post<VerifyNewEmailPayloadInterface>(getUsersURL('verifyNewEmail'), { verifyEmail })
		);

		if (!res.success) {
			throw new Error(res.message || 'Request failed.');
		}

		return;
	}

	get currentUser(): UserInterface | null {
		return this._currentUser;
	}

	set currentUser(value: UserInterface | null) {
		this._currentUser = value;
	}
}
