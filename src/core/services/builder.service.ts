import { Injectable } from '@angular/core';
import { NetworkService } from './network.service';
import { ResponseInterface } from '../../types/response.types';
import { firstValueFrom } from 'rxjs';
import { getBuilderURL } from '../config/api-routes.config';
import { environment } from '../../environments/environment';
import { TemplateInterface } from '../../types/template.types';
import { getTemplatePhotoURL } from '../utils/links.util';

@Injectable({
	providedIn: 'root'
})
export class BuilderService {
	constructor(private _http: NetworkService) {}

	private static _randomUsers(template: TemplateInterface): string {
		if (template.name === 'Protea') {
			return '1 000';
		}

		if (template.name === 'Liatris') {
			return '2 000';
		}

		if (template.name === 'Zinnia') {
			return '8 000';
		}

		if (template.name === 'Genista') {
			return '10 000';
		}

		return '800';
	}

	async getResumePagesAPI(sharingLink: string): Promise<string[] | null> {
		const res: ResponseInterface<string[]> = await firstValueFrom<ResponseInterface<string[]>>(
			this._http.get<{ sharingLink: string }, string[]>(getBuilderURL('shareResume'), { sharingLink })
		);

		if (!res.success || !res.body) {
			return null;
		}

		return res.body.map((value: string) => `${environment.builderApi}/classic_media/${value}`);
	}

	async getTemplatesListAPI(): Promise<TemplateInterface[] | null> {
		const res: ResponseInterface<TemplateInterface[]> = await firstValueFrom<
			ResponseInterface<TemplateInterface[]>
		>(this._http.get<unknown, TemplateInterface[]>(getBuilderURL('templates')));

		if (!res.success || !res.body) {
			return null;
		}

		return res.body.map((value: TemplateInterface) => ({
			...value,
			previewSm: getTemplatePhotoURL(value.previewSm),
			previewXl: getTemplatePhotoURL(value.previewXl),
			users: BuilderService._randomUsers(value)
		}));
	}
}
