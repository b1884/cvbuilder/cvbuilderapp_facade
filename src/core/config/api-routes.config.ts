import { environment } from '../../environments/environment';

const auth = {
	signUp: '/sign-up',
	signIn: '/sign-in',
	signOut: '/sign-out',
	verifyEmail: '/verify-email',
	checkIfEmailExist: '/check-email-exist',
	getLinkedinToken: '/get-linkedin-token'
};

const builder = {
	shareResume: '/share-resume',
	templates: '/templates'
};

const subscription = {
	all: '/all'
};

const users = {
	single: '/single',
	current: '/current',
	verifyNewEmail: '/verify-new-email'
};

export const getAuthURL = (child: keyof typeof auth): string => `${environment.builderApi}/auth${auth[child]}`;

export const getUsersURL = (child: keyof typeof users): string => `${environment.builderApi}/users${users[child]}`;

export const getBuilderURL = (child: keyof typeof builder): string =>
	`${environment.builderApi}/classic-builder${builder[child]}`;

export const getSubscriptionURL = (child: keyof typeof subscription): string =>
	`${environment.builderApi}/subscription${subscription[child]}`;
