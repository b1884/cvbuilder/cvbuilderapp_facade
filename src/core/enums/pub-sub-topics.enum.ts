export enum PubSubTopicsEnum {
	toggleMenu = 'toggleMenu',
	updateMeta = 'updateMeta',
	hamburgerEffects = 'hamburgerEffects',
	changeCvTemplate = 'changeCvTemplate'
}
