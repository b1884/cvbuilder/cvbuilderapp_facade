import { Injectable } from '@angular/core';
import { UserInterface } from '../../types/user.type';
import { UsersService } from '../services/users.service';
import { environment } from '../../environments/environment';
import { CanActivate, UrlTree } from '@angular/router';

@Injectable({
	providedIn: 'root'
})
export class AuthGuard implements CanActivate {
	constructor(private _userService: UsersService) {}

	async canActivate(): Promise<boolean | UrlTree> {
		let currentUser: UserInterface | null;

		try {
			currentUser = await this._userService.currentUserAPI();

			if (!currentUser) {
				return true;
			}

			location.href = environment.builderApp;
			return false;
		} catch (e) {
			console.error(e);
			return true;
		}
	}
}
