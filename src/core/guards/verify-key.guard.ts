import { Injectable } from '@angular/core';
import { SignInResponseInterface } from '../../types/auth.types';
import { AuthService } from '../services/auth.service';
import { environment } from '../../environments/environment';
import { UsersService } from '../services/users.service';
import { LoaderService } from '../services/loader.service';
import { HttpErrorResponse } from '@angular/common/http';
import { ActivatedRouteSnapshot, CanActivate, UrlTree } from '@angular/router';

@Injectable({
	providedIn: 'root'
})
export class VerifyKeyGuard implements CanActivate {
	constructor(
		private _authService: AuthService,
		private _loaderService: LoaderService,
		private _usersService: UsersService
	) {}

	async canActivate(route: ActivatedRouteSnapshot): Promise<boolean | UrlTree> {
		if (!route.queryParams) {
			return true;
		}

		const { magic_link: magicLink, verify_email: verifyEmail } = route.queryParams;

		if (magicLink) {
			let res: SignInResponseInterface;

			try {
				this._loaderService.showLoader();
				res = await this._authService.verifyEmailAPI(magicLink);

				if (res && res.exp) {
					localStorage.setItem('exp', `${res.exp}`);
					location.href = environment.builderApp;
				}

				this._loaderService.hideLoader();
			} catch (e) {
				if (e instanceof HttpErrorResponse) {
					alert(e.error.message);
				}

				this._loaderService.hideLoader();
			}
		}

		if (verifyEmail) {
			try {
				this._loaderService.showLoader();
				await this._usersService.verifyNewEmailAPI(verifyEmail);
				alert('Adresse e-mail confirmée avec succès!');
				this._loaderService.hideLoader();
				return true;
			} catch (e) {
				this._loaderService.hideLoader();
				if (!e) {
					return true;
				}

				if (e instanceof HttpErrorResponse) {
					alert(e.error.message);
				}
			}
		}

		return true;
	}
}
