import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, UrlTree } from '@angular/router';
import { lastValueFrom } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import routes from '../../routes';
import { LANGUAGES } from '../config/enabled-languages.config';
import { LangType } from '../../types/lang.types';

@Injectable({
	providedIn: 'root'
})
export class LanguageGuard implements CanActivate {
	constructor(private _translate: TranslateService, private _router: Router) {}

	private async _setAndNavigate(lang: string): Promise<boolean> {
		localStorage.setItem('lang', lang);
		this._translate.setDefaultLang(lang);
		await lastValueFrom(this._translate.use(lang));
		const appRoutes = routes(lang as LangType);
		await this._router.navigate([appRoutes.landing.main]);
		return false;
	}

	async canActivate(route: ActivatedRouteSnapshot): Promise<boolean | UrlTree> {
		const currentLang = localStorage.getItem('lang');

		if (currentLang && LANGUAGES.includes(currentLang)) {
			return await this._setAndNavigate(currentLang);
		}

		if (route.url && !route.url.length) {
			let browserLang = `${this._translate.getBrowserLang()}`;
			browserLang = LANGUAGES.includes(browserLang) ? browserLang : 'en';

			return await this._setAndNavigate(browserLang);
		}

		return true;
	}
}
