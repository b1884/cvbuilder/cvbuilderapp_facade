import { environment } from '../../environments/environment';
import { LangType } from '../../types/lang.types';

export const getTemplatePhotoURL = (templateLink: string): string =>
	`${environment.builderApi}/classic_previews/${templateLink}`;

export const getLanguageSpecificURL = (frURL: string, enURL: string, lang?: LangType, arURL?: string): string => {
	if (!lang) {
		return frURL;
	}

	switch (lang) {
		case 'fr':
			return frURL;
		case 'en':
			return enURL;
		case 'ar':
			if (!arURL) {
				return frURL;
			}

			return arURL;
		default:
			return frURL;
	}
};
