import { LangType } from '../types/lang.types';
import { getLanguageSpecificURL } from '../core/utils/links.util';

export interface LandingModuleRoutesInterface {
	main: string;
	terms: string;
	faq: string;
	privacy: string;
	about: string;
	templates: string;
	pricing: string;
}

export default (mainPath: string, lang?: LangType): LandingModuleRoutesInterface => ({
	main: `${mainPath}${getLanguageSpecificURL(
		'cv-designer',
		'formatting-a-cv',
		lang,
		'نموذج-سيرة-ذاتية-جاهز-للتعبئة-pdf'
	)}`,
	terms: `${mainPath}${getLanguageSpecificURL('conditions-generales', 'terms-of-service', lang, 'شروط-الاستخدام')}`,
	faq: `${mainPath}${getLanguageSpecificURL('faire-un-cv', 'how-to-make-cv', lang, 'تصميم-سيرة-ذاتية')}`,
	privacy: `${mainPath}${getLanguageSpecificURL(
		'politique-confidentialite',
		'privacy-policy',
		lang,
		'سياسة-الخصوصية'
	)}`,
	about: `${mainPath}${getLanguageSpecificURL('a-propos', 'about-us', lang, 'من-نحن')}`,
	templates: `${mainPath}${getLanguageSpecificURL('modele-cv', 'cv-examples', lang, 'نماذج-سيرة-ذاتية')}`,
	pricing: `${mainPath}${getLanguageSpecificURL('tarifs', 'pricing', lang, 'التعريفة')}`
});
