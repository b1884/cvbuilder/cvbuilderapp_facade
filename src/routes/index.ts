import landing, { LandingModuleRoutesInterface } from './landing.routes';
import publicCvLink from './public-cvlink.routes';
import auth from './auth.routes';
import { LangType } from '../types/lang.types';

export type LandingAppRoutesType = {
	landing: LandingModuleRoutesInterface;
	publicCvlink: { cvPage: (cvId: string) => string };
	auth: { login: string; signup: string };
};

export default (lang: LangType): LandingAppRoutesType => ({
	landing: landing('/', lang),
	publicCvlink: publicCvLink('/s'),
	auth: auth('/auth', lang)
});
