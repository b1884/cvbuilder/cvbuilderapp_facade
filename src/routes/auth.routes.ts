import { LangType } from '../types/lang.types';
import { getLanguageSpecificURL } from '../core/utils/links.util';

export default (mainPath: string, lang?: LangType): { login: string; signup: string } => ({
	login: `${mainPath}/${getLanguageSpecificURL('mon-meilleur-cv', 'cvmaker', lang, 'سيرة-ذاتية-جاهزة')}`,
	signup: `${mainPath}/${getLanguageSpecificURL('creer-un-cv', 'cvbuilder', lang, 'تصميم-سيرة-ذاتية')}`
});
