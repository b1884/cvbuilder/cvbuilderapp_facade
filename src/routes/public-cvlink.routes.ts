export default (mainPath: string): { cvPage: (cvId: string) => string } => ({
	cvPage: (cvId: string): string => `${mainPath}/${cvId}`
});
