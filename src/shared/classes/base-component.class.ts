import { BreakpointObserver } from '@angular/cdk/layout';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslationPathsService } from '../../core/services/translation-paths.service';
import { ServiceLocator } from './service-locator.class';
import appRoutes, { LandingAppRoutesType } from '../../routes';
import { PubSubService } from '../../core/services/pub-sub.service';
import { isPlatformBrowser, Location } from '@angular/common';
import { InjectionToken, PLATFORM_ID } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from '../../core/services/auth.service';
import { LoaderService } from '../../core/services/loader.service';
import { TitleAndMetaService } from '../../core/services/title-and-meta.service';
import { LangType } from '../../types/lang.types';
import { firstValueFrom, lastValueFrom } from 'rxjs';

export abstract class BaseComponent {
	private _isDroppedDown = false;

	private _backDrop!: HTMLElement | undefined;

	private _lang = localStorage.getItem('lang') ? localStorage.getItem('lang') : 'fr';

	private readonly _breakpointObserver: BreakpointObserver;

	private readonly _metaService: TitleAndMetaService;

	private readonly _location: Location;

	private readonly _router: Router;

	private readonly _pubSubService: PubSubService;

	private readonly _t: TranslationPathsService;

	private readonly _translate: TranslateService;

	private readonly _authService: AuthService;

	private readonly _loaderService: LoaderService;

	private readonly _platformId: InjectionToken<any>;

	private _routes!: LandingAppRoutesType;

	protected constructor() {
		this._location = ServiceLocator.injector.get<Location>(Location);
		this._pubSubService = ServiceLocator.injector.get<PubSubService>(PubSubService);
		this._breakpointObserver = ServiceLocator.injector.get<BreakpointObserver>(BreakpointObserver);
		this._t = ServiceLocator.injector.get<TranslationPathsService>(TranslationPathsService);
		this._translate = ServiceLocator.injector.get<TranslateService>(TranslateService);
		this._router = ServiceLocator.injector.get<Router>(Router);
		this._authService = ServiceLocator.injector.get<AuthService>(AuthService);
		this._loaderService = ServiceLocator.injector.get<LoaderService>(LoaderService);
		this._metaService = ServiceLocator.injector.get<TitleAndMetaService>(TitleAndMetaService);
		this._platformId = ServiceLocator.injector.get<InjectionToken<any>>(PLATFORM_ID);

		if (isPlatformBrowser(this._platformId)) {
			window.scroll(0, 0);
		}
	}

	changeLanguage(lang: LangType): void {
		this.translate.use(lang);
		localStorage.setItem('lang', lang);
		this._routes = appRoutes(lang);
		void this.router.navigate([appRoutes(lang).landing.main]);

		if (isPlatformBrowser(this._platformId)) {
			window.scroll(0, 0);
		}
	}

	get metaService(): TitleAndMetaService {
		return this._metaService;
	}

	get loaderService(): LoaderService {
		return this._loaderService;
	}

	get authService(): AuthService {
		return this._authService;
	}

	get translate(): TranslateService {
		return this._translate;
	}

	get isDroppedDown(): boolean {
		return this._isDroppedDown;
	}

	set isDroppedDown(value: boolean) {
		this._isDroppedDown = value;
	}

	get backDrop(): HTMLElement | undefined {
		return this._backDrop;
	}

	set backDrop(value: HTMLElement | undefined) {
		this._backDrop = value;
	}

	get location(): Location {
		return this._location;
	}

	get pubSubService(): PubSubService {
		return this._pubSubService;
	}

	get breakpointObserver(): BreakpointObserver {
		return this._breakpointObserver;
	}

	get router(): Router {
		return this._router;
	}

	get t(): TranslationPathsService {
		return this._t;
	}

	get lang(): string | null {
		return this._lang;
	}

	get routes(): LandingAppRoutesType {
		const currentLang = this.translate.currentLang;
		this._routes = appRoutes(currentLang as LangType);

		return appRoutes(currentLang as LangType);
	}

	toggleDropdown(unfold = false): void {
		if (unfold) {
			this.isDroppedDown = false;

			if (this.backDrop) {
				document.body.removeChild(this.backDrop);
				this.backDrop = undefined;
			}

			return;
		}
		this.isDroppedDown = !this.isDroppedDown;
		if (this.isDroppedDown) {
			this.backDrop = document.createElement('DIV');
			this.backDrop.classList.add('nav-dropdown-backdrop');
			document.body.appendChild(this.backDrop);

			this.backDrop.addEventListener('click', () => {
				this.toggleDropdown();
			});
		}

		if (!this.isDroppedDown && this.backDrop) {
			document.body.removeChild(this.backDrop);
			this.backDrop = undefined;
		}
	}

	async goToBuilder(): Promise<void> {
		if (!this._authService.isTokenValid()) {
			await this._router.navigate([this._routes.auth.login]);
		}

		location.href = this._authService.getBuilderURL();
	}

	handleAccordionClick(elementId: string): void {
		const acc = document.getElementsByClassName('accordion');

		for (const element of Array.from(acc as HTMLCollectionOf<HTMLElement>)) {
			if (element.id !== elementId) {
				element.classList.remove('active');

				const panel = element.nextElementSibling as HTMLElement;
				if (!panel || !panel.style) {
					return;
				}

				panel.style.maxHeight = '';
				continue;
			}

			if (element.classList.contains('active')) {
				element.classList.remove('active');
			} else {
				element.classList.toggle('active');
			}

			const panel = element.nextElementSibling as HTMLElement;
			if (!panel || !panel.style) {
				return;
			}

			if (!panel.style.maxHeight) {
				panel.style.maxHeight = `${panel.scrollHeight}px`;
			} else {
				panel.style.maxHeight = '';
			}
		}
	}

	isRTL(): boolean {
		return localStorage.getItem('lang') === 'ar';
	}

	async setLanguage(activatedRoute: ActivatedRoute): Promise<void> {
		const data = await firstValueFrom(activatedRoute.data);
		if (!data || !data.lang) {
			return;
		}

		const lang = data.lang;

		const currentLang = localStorage.getItem('lang');
		if (!currentLang) {
			localStorage.setItem('lang', lang);
		}

		await lastValueFrom(this.translate.use(lang));
	}
}
