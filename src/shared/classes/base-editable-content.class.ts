import { ElementRef } from '@angular/core';

export abstract class BaseEditableContent {
	private _contentEditable!: ElementRef<HTMLElement>;

	private _isTitleEditable = false;

	blurContent(): null {
		this.isTitleEditable = false;
		const selection = window.getSelection();

		if (selection) {
			selection.removeAllRanges();
		}

		return null;
	}

	handleContentEditable(): void {
		this.isTitleEditable = true;

		if (this.contentEditable) {
			const editableElement: HTMLInputElement = <HTMLInputElement>this.contentEditable.nativeElement;

			setTimeout(() => {
				editableElement.focus();

				editableElement.onfocus = () => {
					document.execCommand('selectAll', false, undefined);
				};

				editableElement.onblur = this.blurContent.bind(this);
			});
		}
	}

	get contentEditable(): ElementRef<HTMLElement> {
		return this._contentEditable;
	}

	set contentEditable(value: ElementRef<HTMLElement>) {
		this._contentEditable = value;
	}

	get isTitleEditable(): boolean {
		return this._isTitleEditable;
	}

	set isTitleEditable(value: boolean) {
		this._isTitleEditable = value;
	}
}
