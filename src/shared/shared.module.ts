import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { pluck } from 'rxjs/operators';
import { asapScheduler, Observable, scheduled } from 'rxjs';
import lng from '../locals/fr';
import { CarouselModule } from 'ngx-owl-carousel-o';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgxPopperModule } from 'ngx-popper';

export class WebpackTranslateLoader implements TranslateLoader {
	getTranslation(lang: string): Observable<typeof lng> {
		// eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
		return scheduled<typeof lng>(import(`../locals/${lang}/index.ts`), asapScheduler).pipe(
			pluck('default')
		) as Observable<typeof lng>;
	}
}

@NgModule({
	imports: [
		CommonModule,
		CarouselModule,
		HttpClientModule,
		FormsModule,
		ReactiveFormsModule,
		NgxPopperModule.forRoot({
			disableDefaultStyling: true,
			applyClass: 'popper-tooltip-styles'
		}),
		TranslateModule.forChild({
			loader: {
				provide: TranslateLoader,
				useClass: WebpackTranslateLoader
			}
		})
	],
	exports: [TranslateModule, CarouselModule, FormsModule, ReactiveFormsModule, NgxPopperModule]
})
export class SharedModule {}
