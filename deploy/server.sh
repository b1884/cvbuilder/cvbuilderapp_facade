cd ~/cv-template.co/cvbuilderapp_facade
git checkout master
git pull origin master

NODE_ENV="whatever_not_prod" yarn install
yarn prod:build:ssr
yarn prerender
pm2 restart cvbuilderapp_facade || pm2 start ~/cv-template.co/cvbuilderapp_facade/dist/cvbuilderapp_facade/server/main.js --name "cvbuilderapp_facade"
