import 'zone.js/dist/zone-node';

import { ngExpressEngine } from '@nguniversal/express-engine';
import * as express from 'express';
import { join } from 'path';
import compression from 'compression';

import { APP_BASE_HREF } from '@angular/common';
import { existsSync, readFileSync } from 'fs';

import { createWindow } from 'domino';
import 'localstorage-polyfill';

const distFolder = join(process.cwd(), 'dist/cvbuilderapp_facade/browser');

const htmlTemplate = readFileSync(join(distFolder, 'index.html')).toString();
const window = createWindow(htmlTemplate);

// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
global['window'] = window;
global['document'] = window.document;
global['localStorage'] = localStorage;
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
global['window']['cookieconsent'] = {
	initialise: () => {
		console.warn('Cookie consent is not working on server side');
	}
};

import { AppServerModule } from './src/main.server';

// The Express app is exported so that it can be used by serverless Functions.
export const app = (): express.Express => {
	const server = express();
	const indexHtml = existsSync(join(distFolder, 'index.original.html')) ? 'index.original.html' : 'index';

	// Our Universal express-engine (found @ https://github.com/angular/universal/tree/master/modules/express-engine)

	server.use(compression({ level: 9 }));
	server.engine(
		'html',
		ngExpressEngine({
			bootstrap: AppServerModule
		})
	);

	server.set('view engine', 'html');
	server.set('views', distFolder);

	// Example Express Rest API endpoints
	// server.get('/api/**', (req, res) => { });
	// Serve static files from /browser
	server.get(
		'*.*',
		express.static(distFolder, {
			maxAge: '1y'
		})
	);

	// All regular routes use the Universal engine
	server.get('*', (req, res) => {
		res.render(indexHtml, { req, providers: [{ provide: APP_BASE_HREF, useValue: req.baseUrl }] });
	});

	return server;
};

const run = (): void => {
	const port = process.env['CVBUILDER_FACADE_PORT'] || 6969;

	// Start up the Node server
	const server = app();
	server.listen(port, () => {
		console.log(`Node Express server for cvbuilder facade app listening on http://localhost:${port}`);
	});
};

// Webpack will replace 'require' with '__webpack_require__'
// '__non_webpack_require__' is a proxy to Node 'require'
// The below code is to ensure that the server is run only when not requiring the bundle.
// eslint-disable-next-line @typescript-eslint/naming-convention
declare const __non_webpack_require__: NodeRequire;
const mainModule = __non_webpack_require__.main;
const moduleFilename = (mainModule && mainModule.filename) || '';
if (moduleFilename === __filename || moduleFilename.includes('iisnode')) {
	run();
}

export * from './src/main.server';
